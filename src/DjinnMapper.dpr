program DjinnMapper;

uses
  Forms,
  DTMmain in 'DTMmain.pas' {DjinnTileMapper},
  inputbox in 'inputbox.pas' {InputForm},
  About in 'About.pas' {AboutForm},
  Search in 'Search.pas' {SearchForm},
  crtrd in 'crtrd.pas' {crtransform},
  srres in 'srres.pas' {searchresform},
  tmform in 'tmform.pas' {tilemapform},
  teditf in 'teditf.pas' {teditform},
  dataf in 'dataf.pas' {dataform};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'Djinn Tile Mapper v1.4.9.9';
  Application.CreateForm(TDjinnTileMapper, DjinnTileMapper);
  Application.CreateForm(TInputForm, InputForm);
  Application.CreateForm(TAboutForm, AboutForm);
  Application.CreateForm(TSearchForm, SearchForm);
  Application.CreateForm(Tcrtransform, crtransform);
  Application.CreateForm(Tsearchresform, searchresform);
  Application.CreateForm(Ttilemapform, tilemapform);
  Application.CreateForm(Tteditform, teditform);
  Application.CreateForm(Tdataform, dataform);
  Application.Run;
end.
