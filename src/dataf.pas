unit dataf;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin, ExtCtrls, Buttons;

type
  Tdataform = class(TForm)
    otherpan: TPanel;
    dRightBtn: TSpeedButton;
    dLeftBtn: TSpeedButton;
    WLab: TLabel;
    hlab: TLabel;
    Label1: TLabel;
    Label5: TLabel;
    SignL: TLabel;
    Panel1: TPanel;
    DataMap: TImage;
    DataScroll: TScrollBar;
    WidthEdit: TSpinEdit;
    HeightEdit: TSpinEdit;
    hexed: TEdit;
    Label2: TLabel;
    procedure dLeftBtnClick(Sender: TObject);
    procedure dRightBtnClick(Sender: TObject);
    procedure WidthEditChange(Sender: TObject);
    procedure WidthEditKeyPress(Sender: TObject; var Key: Char);
    procedure HeightEditChange(Sender: TObject);
    procedure hexedChange(Sender: TObject);
    procedure hexedClick(Sender: TObject);
    procedure hexedKeyPress(Sender: TObject; var Key: Char);
    procedure DataScrollChange(Sender: TObject);
    procedure DataMapClick(Sender: TObject);
    procedure DataMapMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dataform: Tdataform;

implementation

uses DTMmain;

{$R *.dfm}

procedure Tdataform.dLeftBtnClick(Sender: TObject);
begin
 DataScroll.position := ROMdatapos - 1;
end;

procedure Tdataform.dRightBtnClick(Sender: TObject);
begin
 DataScroll.position := ROMdatapos + 1;
end;

procedure Tdataform.WidthEditChange(Sender: TObject);
begin
 djinntilemapper.dWidth := Widthedit.Value
end;

procedure Tdataform.WidthEditKeyPress(Sender: TObject; var Key: Char);
begin
 If Key in ['-', ',', '.', '+', '='] then Key := #0;
end;

procedure Tdataform.HeightEditChange(Sender: TObject);
begin
 djinntilemapper.dHeight := Heightedit.Value
end;

procedure Tdataform.hexedChange(Sender: TObject);
Var i: Byte; s: String; err: Boolean;
begin
 If HexEd.Tag = 1 then
 begin
  s := HexEd.Text;
  If Length(s) > 2 then SetLength(s, 2);
  For i := 1 to Length(s) do
   If not (s[i] in ['0'..'9', 'A'..'F']) then s[i] := '0';
  HexEd.Text := s;
  If Length(s) = 2 then
  begin
   i := HexVal(s, err);
   RomData^[romDatapos + CDPos] := i;
   Inc(CDpos);
   If romDataPos + CDpos = ROMsize then Dec(CDPos);
   Label1.Caption := '�������: ' + IntToHex(romDataPos + CDPos, 8);
   If (CDPos >= djinntilemapper.dWidth * djinntilemapper.dHeight) then
   begin
    Dec(CDpos);
    DataScroll.Position := DataScroll.Position + 1;
  If not tTblOpened then djinntilemapper.Bank := RomData^[romDatapos + CDPos] Else
                         djinntilemapper.Bank := TileTable[RomData^[romDatapos + CDPos]];
   end Else
   begin
  If not tTblOpened then djinntilemapper.Bank := RomData^[romDatapos + CDPos] Else
                         djinntilemapper.Bank := TileTable[RomData^[romDatapos + CDPos]];
   end;
   HexEd.SelectAll;
  end;
 end;
end;


procedure Tdataform.hexedClick(Sender: TObject);
begin
 If HexEd.Enabled then HexEd.SelectAll;
end;

procedure Tdataform.hexedKeyPress(Sender: TObject; var Key: Char);
begin
 Key := UpCase(Key);
 If not (Key in ['0'..'9', 'A'..'F', #8]) then Key := #0;
end;

procedure Tdataform.DataScrollChange(Sender: TObject);
begin
 djinntilemapper.RDatapos := LongInt(DataScroll.position);
 Label2.Caption := '��������� �������: ' + IntToHex(romDataPos + CDPos, 8); 
end;

procedure Tdataform.DataMapClick(Sender: TObject);
Var i: Integer;
begin
 If ROMopened then
 begin
  i := CDpos;
  CDpos := (dmMouseY div 16) * djinntilemapper.dWidth + dmMousex div 16;
  datascroll.SetFocus;
  If ShiftPrsd and (CDPos > i) then
  begin
   MAXSLEN := CDpos - i + 1;
   djinntilemapper.MaxLenLab.Caption := '������������ �����: ' +
    IntToStr(MaxSlen) + ' = h0' + IntToHex(maxSlen, 3);
   djinntilemapper.SpinEdit1.Value := MaxSLen;
   CDpos := i;
   Exit;
  end;
  Label1.Caption := '�������: ' + IntToHex(romDataPos + CDPos, 8);
  Label2.Caption := '��������� �������: ' + IntToHex(romDataPos + CDPos, 8);
  If not tTblOpened then djinntilemapper.Bank := RomData^[romDatapos + CDPos] Else
                         djinntilemapper.Bank := TileTable[RomData^[romDatapos + CDPos]];
 end;
end;


procedure Tdataform.DataMapMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
Var i: Longint;
begin
 dmMouseX := X;
 dmMouseY := Y;
 shiftprsd := ssShift in Shift;
 If ROMopened then
 begin
  i := romDataPos + (dmMouseY div 16) * djinntilemapper.dWidth + dmMousex div 16;
  label5.Caption := IntToHex(ROMData^[i], 2) + '=' + IntToStr(ROMData^[i]);
  Label1.Caption := '�������: ' + IntToHex(i, 8);
 end;
end;

end.
