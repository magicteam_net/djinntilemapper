object InputForm: TInputForm
  Left = 448
  Top = 344
  BorderStyle = bsDialog
  Caption = 'Djinn Tile Mapper'
  ClientHeight = 76
  ClientWidth = 303
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ifLabel: TLabel
    Left = 8
    Top = 8
    Width = 3
    Height = 13
  end
  object ifEdit: TEdit
    Left = 8
    Top = 24
    Width = 289
    Height = 21
    TabOrder = 0
    OnChange = ifEditChange
    OnKeyPress = ifEditKeyPress
  end
  object ifOKbtn: TButton
    Left = 8
    Top = 48
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    TabOrder = 1
    OnClick = ifOKbtnClick
  end
  object ifCnclbtn: TButton
    Left = 88
    Top = 48
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 2
    OnClick = ifCnclbtnClick
  end
end
