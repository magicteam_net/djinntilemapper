unit tmform;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin, ExtCtrls, Buttons;

type
  Ttilemapform = class(TForm)
    LeftBtn: TSpeedButton;
    RightBtn: TSpeedButton;
    MinusBtn: TSpeedButton;
    PlusBtn: TSpeedButton;
    TileMap: TImage;
    mbanklab: TLabel;
    TMapScroll: TScrollBar;
    c16x16: TCheckBox;
    Codecbox: TComboBox;
    tlwidth: TSpinEdit;
    tlheight: TSpinEdit;
    twidth: TSpinEdit;
    theight: TSpinEdit;
    procedure LeftBtnClick(Sender: TObject);
    procedure RightBtnClick(Sender: TObject);
    procedure MinusBtnClick(Sender: TObject);
    procedure PlusBtnClick(Sender: TObject);
    procedure CodecboxChange(Sender: TObject);
    procedure CodecboxKeyPress(Sender: TObject; var Key: Char);
    procedure TileMapClick(Sender: TObject);
    procedure TileMapDblClick(Sender: TObject);
    procedure TileMapMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure c16x16Click(Sender: TObject);
    procedure tlheightChange(Sender: TObject);
    procedure tlwidthChange(Sender: TObject);
    procedure TMapScrollChange(Sender: TObject);
    procedure twidthChange(Sender: TObject);
    procedure theightChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  tilemapform: Ttilemapform;

implementation

{$R *.dfm}

uses dtmmain, teditf;

procedure Ttilemapform.LeftBtnClick(Sender: TObject);
begin
 TMapScroll.position := djinntilemapper.ROMpos - (8 * (tlwidthv * tlheightv)* bsz[djinntilemapper.Codec]
                      + 3 * (8 *(tlwidthv * tlheightv)* bsz[djinntilemapper.Codec]) * Byte(b16x16));
end;

procedure Ttilemapform.RightBtnClick(Sender: TObject);
begin
 TMapScroll.position := djinntilemapper.ROMpos + 8 *(tlwidthv * tlheightv)* bsz[djinntilemapper.Codec] + 3 * (8 *(tlwidthv * tlheightv)* bsz[djinntilemapper.Codec]) * Byte(b16x16);
end;

procedure Ttilemapform.MinusBtnClick(Sender: TObject);
begin
 TMapScroll.position := djinntilemapper.ROMpos - 1;
end;

procedure Ttilemapform.PlusBtnClick(Sender: TObject);
begin
 TMapScroll.position := djinntilemapper.ROMpos + 1;
end;

procedure Ttilemapform.CodecboxChange(Sender: TObject);
Var cdt: TCodec;
begin
 If (CodecBox.ItemIndex >= 0) then
 begin
  Byte(cdt) := CodecBox.ItemIndex;
  If cdt <> djinntilemapper.Codec then djinntilemapper.Codec := cdt;
  If codecbox.Text <> codecbox.Items.Strings[codecbox.ItemIndex]  then
   codecbox.Text := codecbox.Items.Strings[codecbox.ItemIndex];
 end;
end;

procedure Ttilemapform.CodecboxKeyPress(Sender: TObject; var Key: Char);
begin
 Key := #0;
end;

procedure Ttilemapform.TileMapClick(Sender: TObject);
var b: byte; xx, xu, yy, yu: integer;
begin
if (tlwidthv = 1) and (tlheightv = 1) then
begin
  djinntilemapper.Bank := (tmMouseY div (tileh * 2)) * 16 +
  tmMousex div (tilew * 2);
{  showmessage(inttostr(djinntilemapper.Bank));}
 end
 else
 begin
  b := 0;
  for yy := 0 to 16 div tlheightv - 1 do
   for xx := 0 to 16 div tlwidthv - 1 do
   begin
    for yu := 0 to tlheightv - 1 do
     for xu := 0 to tlwidthv - 1 do
     begin
     if ((xx *  tlwidthv + xu) = tmMousex div 16) and
        ((yy * tlheightv + yu) = tmMouseY div 16) then
     begin
      djinntilemapper.bank := b;
 mBankLab.Caption := '����: ' +
  IntToHex(b, 2) + ' = ' + IntToStr(b);
 If tmapscroll.enabled then TMapScroll.SetFocus;
      exit;
     end;
     inc(b);
    end;
   end;
 end;
 If tmapscroll.enabled then TMapScroll.SetFocus;
end;


procedure Ttilemapform.TileMapDblClick(Sender: TObject);
Var i: Byte; b: boolean;
begin
 If ROMOpened then
 begin
  Saved := False;
{  djinntilemapper.Caption := Capt + ' (' + inttohex(DataPos, 6) +
            ' / ' + inttohex(ROMsize, 6) + ') - ' + fname +
            SS[Saved];}
  caption := '����� ������ - [' + inttohex(DataPos, 8) +
            ' / ' + inttohex(ROMsize, 8) + ']';
  If not tTblOpened then ROMdata^[romDataPos + CDpos] := djinntilemapper.Bank Else
  begin
   b := False;
   For i := 0 to 255 do If TileTable[i] = djinntilemapper.Bank then
   begin
    b := True;
    Break;
   end;
   If B then ROMdata^[romDataPos + CDpos] := i Else
             ROMdata^[romDataPos + CDpos] := 0;
  end;
  djinntilemapper.DataMapDraw;
 end;
end;


procedure Ttilemapform.TileMapMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
Var b: Byte; yy, xx, xu, yu: integer;
begin
 tmMouseX := X;
 tmMouseY := Y;
 if (tlwidthv = 1) and (tlheightv = 1) then
  b := (tmMouseY div (tileh * 2)) * 16 +
  tmMousex div (tileh * 2)
  else
 begin
  b := 0;
  for yy := 0 to 16 div tlheightv - 1 do
   for xx := 0 to 16 div tlwidthv - 1 do
   begin
    for yu := 0 to tlheightv - 1 do
     for xu := 0 to tlwidthv - 1 do
     begin
     if ((xx *  tlwidthv + xu) = x div 16) and
        ((yy * tlheightv + yu) = y div 16) then
     begin
 mBankLab.Caption := '����: ' +
  IntToHex(b, 2) + ' = ' + IntToStr(b);
      exit;
     end;
     inc(b);
    end;
   end;
 end;
 mBankLab.Caption := '����: ' +
  IntToHex(b, 2) + ' = ' + IntToStr(b);
end;


procedure Ttilemapform.c16x16Click(Sender: TObject);
begin
 djinntilemapper.t16x16 := c16x16.Checked;
 if c16x16.Checked then
 begin
  tilew := 8;
  tileh := 8;
  tilemap.Width := tilew * 32;
  tilemap.Height := tileh * 32;
  tmapscroll.Left := tilemap.Left + tilemap.Width;
  tmapscroll.Height := tilemap.Height;
  mbanklab.Top := tilemap.Top + tilemap.Height + 5;
  c16x16.Top := tilemap.Top + tilemap.Height + 5;
  tlwidth.Top := tilemap.Top + tilemap.Height + 25;
  tlheight.Top := tilemap.Top + tilemap.Height + 25;
  twidth.Top := tilemap.Top + tilemap.Height + 25;
  theight.Top := tilemap.Top + tilemap.Height + 25;
  bmap.Width := 256;
  bmap.Height := 256;
  bmap.Canvas.Brush.Color := 0;
  bmap.Canvas.FillRect(bounds(0, 0, bmap.width, bmap.height));
  djinntilemapper.fill(tilemap, 0);
  If ROMData <> NIL then djinntilemapper.drawtilemap;
 end Else
 begin
  tilew := twidth.Value;
  tileh := theight.Value;
  tilemap.Width := tilew * 32;
  tilemap.Height := tileh * 32;
  tmapscroll.Left := tilemap.Left + tilemap.Width;
  tmapscroll.Height := tilemap.Height;
  mbanklab.Top := tilemap.Top + tilemap.Height + 5;
  c16x16.Top := tilemap.Top + tilemap.Height + 5;
  tlwidth.Top := tilemap.Top + tilemap.Height + 25;
  tlheight.Top := tilemap.Top + tilemap.Height + 25;
  twidth.Top := tilemap.Top + tilemap.Height + 25;
  theight.Top := tilemap.Top + tilemap.Height + 25;
  bmap.Width := tilew * 16;
  bmap.Height := tileh * 16;
  bmap.Canvas.Brush.Color := 0;
  bmap.Canvas.FillRect(bounds(0, 0, bmap.width, bmap.height));
  djinntilemapper.fill(tilemap, 0);
  If ROMData <> NIL then djinntilemapper.drawtilemap;
 end;
end;

procedure Ttilemapform.tlheightChange(Sender: TObject);
var tb: tbitmap;
begin
 tlwidthv := tlwidth.Value;
 tlheightv := tlheight.Value;
 if (tlwidthv > 1) or ((tlheightv > 1)) then
 begin
  tilew := 8;
  tileh := 8;
  tilemap.Width := tilew * 32;
  tilemap.Height := tileh * 32;
  tmapscroll.Left := tilemap.Left + tilemap.Width;
  tmapscroll.Height := tilemap.Height;
  mbanklab.Top := tilemap.Top + tilemap.Height + 5;
  c16x16.Top := tilemap.Top + tilemap.Height + 5;
  tlwidth.Top := tilemap.Top + tilemap.Height + 25;
  tlheight.Top := tilemap.Top + tilemap.Height + 25;
  twidth.Top := tilemap.Top + tilemap.Height + 25;
  theight.Top := tilemap.Top + tilemap.Height + 25;
  bmap.Width := 128 + 128 * byte(c16x16.Checked);
  bmap.Height := 128 + 128 * byte(c16x16.Checked);
  bmap.Canvas.Brush.Color := 0;
  bmap.Canvas.FillRect(bounds(0, 0, bmap.width, bmap.height));
  djinntilemapper.fill(tilemap, 0);
 end Else
 begin
  tilew := twidth.Value;
  tileh := theight.Value;
  tilemap.Width := tilew * 32;
  tilemap.Height := tileh * 32;
  tmapscroll.Left := tilemap.Left + tilemap.Width;
  tmapscroll.Height := tilemap.Height;
  mbanklab.Top := tilemap.Top + tilemap.Height + 5;
  c16x16.Top := tilemap.Top + tilemap.Height + 5;
  tlwidth.Top := tilemap.Top + tilemap.Height + 25;
  tlheight.Top := tilemap.Top + tilemap.Height + 25;
  twidth.Top := tilemap.Top + tilemap.Height + 25;
  theight.Top := tilemap.Top + tilemap.Height + 25;
  bmap.Width := tilew * 16;
  bmap.Height := tileh * 16;
  bmap.Canvas.Brush.Color := 0;
  bmap.Canvas.FillRect(bounds(0, 0, bmap.width, bmap.height));
  djinntilemapper.fill(tilemap, 0);
 end;
 tb := tbitmap.Create;
 tb.Width := 1;
 tb.Height := 1;
 tb.Canvas.Pixels[0,0] := 0;
 tilemap.Canvas.stretchdraw(bounds(0, 0, 256, 256), tb);
 tb.Free;
 djinntilemapper.DrawTileMap;
end;


procedure Ttilemapform.tlwidthChange(Sender: TObject);
var tb: tbitmap;
begin
 tlwidthv := tlwidth.Value;
 tlheightv := tlheight.Value;
 if (tlwidthv > 1) or ((tlheightv > 1)) then
 begin
  tilew := 8;
  tileh := 8;
  tilemap.Width := tilew * 32;
  tilemap.Height := tileh * 32;
  tmapscroll.Left := tilemap.Left + tilemap.Width;
  tmapscroll.Height := tilemap.Height;
  mbanklab.Top := tilemap.Top + tilemap.Height + 5;
  c16x16.Top := tilemap.Top + tilemap.Height + 5;
  tlwidth.Top := tilemap.Top + tilemap.Height + 25;
  tlheight.Top := tilemap.Top + tilemap.Height + 25;
  twidth.Top := tilemap.Top + tilemap.Height + 25;
  theight.Top := tilemap.Top + tilemap.Height + 25;
  bmap.Width := 128 + 128 * byte(c16x16.Checked);
  bmap.Height := 128 + 128 * byte(c16x16.Checked);
  bmap.Canvas.Brush.Color := 0;
  bmap.Canvas.FillRect(bounds(0, 0, bmap.width, bmap.height));
  djinntilemapper.fill(tilemap, 0);
 end Else
 begin
  tilew := twidth.Value;
  tileh := theight.Value;
  tilemap.Width := tilew * 32;
  tilemap.Height := tileh * 32;
  tmapscroll.Left := tilemap.Left + tilemap.Width;
  tmapscroll.Height := tilemap.Height;
  mbanklab.Top := tilemap.Top + tilemap.Height + 5;
  c16x16.Top := tilemap.Top + tilemap.Height + 5;
  tlwidth.Top := tilemap.Top + tilemap.Height + 25;
  tlheight.Top := tilemap.Top + tilemap.Height + 25;
  twidth.Top := tilemap.Top + tilemap.Height + 25;
  theight.Top := tilemap.Top + tilemap.Height + 25;
  bmap.Width := tilew * 16;
  bmap.Height := tileh * 16;
  bmap.Canvas.Brush.Color := 0;
  bmap.Canvas.FillRect(bounds(0, 0, bmap.width, bmap.height));
  djinntilemapper.fill(tilemap, 0);
 end;
 tb := tbitmap.Create;
 tb.Width := 1;
 tb.Height := 1;
 tb.Canvas.Pixels[0,0] := 0;
 tilemap.Canvas.stretchdraw(bounds(0, 0, 256, 256), tb);
 tb.Free;
 djinntilemapper.DrawTileMap;
end;


procedure Ttilemapform.TMapScrollChange(Sender: TObject);
begin
 djinntilemapper.Rompos := LongInt(TMapScroll.position);
end;

procedure Ttilemapform.twidthChange(Sender: TObject);
begin
 if not romopened then exit;
 tilew := twidth.Value;
 tilemap.Width := tilew * 32;
 tilemap.Height := tileh * 32;
 tmapscroll.Left := tilemap.Left + tilemap.Width;
 tmapscroll.Height := tilemap.Height;
 mbanklab.Top := tilemap.Top + tilemap.Height + 5;
 c16x16.Top := tilemap.Top + tilemap.Height + 5;
 tlwidth.Top := tilemap.Top + tilemap.Height + 25;
 tlheight.Top := tilemap.Top + tilemap.Height + 25;
 twidth.Top := tilemap.Top + tilemap.Height + 25;
 theight.Top := tilemap.Top + tilemap.Height + 25;
 bmap.Width := tilew * 16;
 bmap.Height := tileh * 16;
 bmap.Canvas.Brush.Color := 0;
 bmap.Canvas.FillRect(bounds(0, 0, bmap.width, bmap.height));
 djinntilemapper.fill(tilemap, 0);
 if (tilew > 8) or (tileh > 8) then
 begin
  btile.Width := 16;
  btile.Height := 16;
 end Else
 begin
  btile.Width := 8;
  btile.Height := 8;
 end;
 if left + width >= screen.Width then left := screen.Width - width;
 djinntilemapper.drawtilemap;
end;

procedure Ttilemapform.theightChange(Sender: TObject);
begin
 if not romopened then exit;
 tileh := theight.Value;
 tilemap.Width := tilew * 32;
 tilemap.Height := tileh * 32;
 tmapscroll.Left := tilemap.Left + tilemap.Width;
 tmapscroll.Height := tilemap.Height;
 mbanklab.Top := tilemap.Top + tilemap.Height + 5;
 c16x16.Top := tilemap.Top + tilemap.Height + 5;
 tlwidth.Top := tilemap.Top + tilemap.Height + 25;
 tlheight.Top := tilemap.Top + tilemap.Height + 25;
 twidth.Top := tilemap.Top + tilemap.Height + 25;
 theight.Top := tilemap.Top + tilemap.Height + 25;
 bmap.Width := tilew * 16;
 bmap.Height := tileh * 16;
 bmap.Canvas.Brush.Color := 0;
 bmap.Canvas.FillRect(bounds(0, 0, bmap.width, bmap.height));
 djinntilemapper.fill(tilemap, 0);
 if (tilew > 8) or (tileh > 8) then
 begin
  btile.Width := 16;
  btile.Height := 16;
 end Else
 begin
  btile.Width := 8;
  btile.Height := 8;
 end;
 djinntilemapper.drawtilemap;
 if left + width >= screen.Width then left := screen.Width - width;
end;

end.
