unit inputbox;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TInputForm = class(TForm)
    ifEdit: TEdit;
    ifOKbtn: TButton;
    ifCnclbtn: TButton;
    ifLabel: TLabel;
    procedure ifOKbtnClick(Sender: TObject);
    procedure ifCnclbtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ifEditKeyPress(Sender: TObject; var Key: Char);
    procedure ifEditChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  InputForm: TInputForm;

Type Tcharset = set of Char;

Function InputBx(ttl, Labl: String; Charmap: tcharset; ml: Integer): String;
function iokresult: boolean;

implementation

{$R *.dfm}

Var ok: boolean;

var chs: TCharset;

Function InputBx(ttl, Labl: String; Charmap: tcharset; ml: Integer): String;
begin
 chs := Charmap;
 inputform.Caption := ttl;
 inputform.ifLabel.Caption := Labl;
 inputform.ifEdit.MaxLength := ml;
 inputform.ShowModal;
 If ok then Result := inputform.ifEdit.text Else Result := '';
end;

procedure TInputForm.ifOKbtnClick(Sender: TObject);
begin
 ok := true;
 close;
end;

procedure TInputForm.ifCnclbtnClick(Sender: TObject);
begin
 close;
end;

procedure TInputForm.FormShow(Sender: TObject);
begin
 ok := false;
 inputform.ifEdit.Text := '';
 ifEdit.SetFocus;
 ifOkBtn.Enabled := False;
end;

procedure TInputForm.ifEditKeyPress(Sender: TObject; var Key: Char);
begin
 If Key = #13 then
 begin
  key := #0;
  If ifOkBtn.Enabled then
  begin
   Ok := True;
   Close;
  end;
 end;
 If not (key in ChS) then key := #0;
end;

procedure TInputForm.ifEditChange(Sender: TObject);
begin
 ifOkbtn.Enabled := (ifEdit.Text <> '');
end;

function iokresult: boolean;
begin
 result := ok;
end;

end.
