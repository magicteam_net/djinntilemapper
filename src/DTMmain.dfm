object DjinnTileMapper: TDjinnTileMapper
  Left = 229
  Top = 186
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  ClientHeight = 72
  ClientWidth = 538
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object TBLnamel: TLabel
    Left = 3
    Top = 48
    Width = 39
    Height = 11
    Caption = #1058#1072#1073#1083#1080#1094#1072':'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -9
    Font.Name = 'Small Fonts'
    Font.Style = []
    ParentFont = False
  end
  object LenLab: TLabel
    Left = 120
    Top = 5
    Width = 87
    Height = 13
    Caption = #1044#1083#1080#1085#1072': 0 = h0000'
  end
  object MaxLenLab: TLabel
    Left = 304
    Top = 5
    Width = 164
    Height = 13
    Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1072#1103' '#1076#1083#1080#1085#1072': 1 = h0001'
  end
  object Label4: TLabel
    Left = 3
    Top = 60
    Width = 71
    Height = 11
    Caption = #1058#1072#1073#1083#1080#1094#1072' '#1090#1072#1081#1083#1086#1074':'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -9
    Font.Name = 'Small Fonts'
    Font.Style = []
    ParentFont = False
  end
  object DataEdit: TEdit
    Left = 0
    Top = 24
    Width = 537
    Height = 21
    Color = clBtnFace
    Enabled = False
    MaxLength = 10000
    TabOrder = 0
    OnChange = DataEditChange
    OnKeyPress = DataEditKeyPress
  end
  object PasteBtn: TButton
    Left = 56
    Top = 2
    Width = 57
    Height = 22
    Caption = #1042#1089#1090#1072#1074#1080#1090#1100
    Enabled = False
    TabOrder = 2
    OnClick = PasteBtnClick
  end
  object GetBtn: TButton
    Left = 0
    Top = 2
    Width = 54
    Height = 22
    Caption = #1048#1079#1074#1083#1077#1095#1100
    Enabled = False
    TabOrder = 1
    OnClick = GetBtnClick
  end
  object SpinEdit1: TSpinEdit
    Left = 488
    Top = 0
    Width = 49
    Height = 22
    AutoSize = False
    MaxLength = 3
    MaxValue = 960
    MinValue = 1
    TabOrder = 3
    Value = 1
    OnChange = SpinEdit1Change
  end
  object OpenROM: TOpenDialog
    DefaultExt = '.nes'
    Filter = 
      #1042#1089#1077' '#1092#1086#1088#1084#1072#1090#1099' (*.nes; *.gb; *.gbc; *.gba; *.bin; *.smc; *.fig; *.c' +
      'hr; *.sms)|*.nes; *.gb; *.gbc; *.gba; *.bin; *.smc; *.fig; *.chr' +
      '; *.sms|NES ROM (*.nes)|*.nes|Game Boy ROM  (*.gb; *.gbc; *.gba)' +
      '|*.gb; *.gbc; *.gba|SNES ROM (*.smc; *.fig)|*.smc; *.fig|Sega RO' +
      'M (*.bin)|*.bin|SMS/SGG ROM (*.sms; *.gg)|*.sms; *.gg|'#1042#1089#1077' '#1092#1072#1081#1083#1099' ' +
      '(*.*)|*.*'
    Title = #1047#1072#1075#1088#1091#1079#1082#1072' ROM'#1072
    Left = 200
    Top = 48
  end
  object SaveROM: TSaveDialog
    DefaultExt = '.nes'
    Filter = 
      #1042#1089#1077' '#1092#1086#1088#1084#1072#1090#1099'|*.nes; *.gb; *.gbc; *.gba; *.bin; *.smc; *.fig; *.ch' +
      'r; *.sms|NES ROM (*.nes)|*.nes|Game Boy ROM  (*.gb; *.gbc; *.gba' +
      ')|*.gb; *.gbc; *.gba|SNES ROM (*.smc; *.fig)|*.smc; *.fig|Sega R' +
      'OM (*.bin)|*.bin|SMS ROM (*.sms)|*.sms|'#1042#1089#1077' '#1092#1072#1081#1083#1099' (*.*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = #1057#1086#1093#1088#1072#1085#1077#1085#1080#1077' ROM'#1072
    Left = 296
    Top = 48
  end
  object MainMenu: TMainMenu
    Left = 360
    Top = 48
    object FileMenu: TMenuItem
      Caption = 'ROM'
      object OpenItem: TMenuItem
        Caption = #1054#1090#1082#1088#1099#1090#1100'...'
        ShortCut = 114
        OnClick = OpenItemClick
      end
      object ReloadItem: TMenuItem
        Caption = #1055#1077#1088#1077#1079#1072#1075#1088#1091#1079#1080#1090#1100
        Enabled = False
        ShortCut = 118
        OnClick = ReloadItemClick
      end
      object SaveItem: TMenuItem
        Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
        Enabled = False
        ShortCut = 113
        OnClick = SaveItemClick
      end
      object SaveASitem: TMenuItem
        Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1082#1072#1082'...'
        Enabled = False
        ShortCut = 16467
        OnClick = SaveASitemClick
      end
      object N15: TMenuItem
        Caption = '-'
      end
      object ExitItem: TMenuItem
        Caption = #1042#1099#1093#1086#1076
        ShortCut = 121
        OnClick = ExitItemClick
      end
    end
    object TableMenu: TMenuItem
      Caption = #1058#1072#1073#1083#1080#1094#1099
      object MkTmplItem: TMenuItem
        Caption = #1057#1086#1079#1076#1072#1090#1100' '#1096#1072#1073#1083#1086#1085' '#1090#1072#1073#1083#1080#1094#1099'...'
        ShortCut = 16461
        OnClick = MkTmplItemClick
      end
      object N16: TMenuItem
        Caption = '-'
      end
      object OpenTblItem: TMenuItem
        Caption = #1054#1090#1082#1088#1099#1090#1100' '#1090#1072#1073#1083#1080#1094#1091'...'
        ShortCut = 16463
        OnClick = OpenTblItemClick
      end
      object Rldtblitem: TMenuItem
        Caption = #1055#1077#1088#1077#1079#1072#1075#1088#1091#1079#1080#1090#1100' '#1090#1072#1073#1083#1080#1094#1091
        Enabled = False
        ShortCut = 123
        OnClick = RldtblitemClick
      end
      object N17: TMenuItem
        Caption = '-'
      end
      object ottnl: TMenuItem
        Caption = #1054#1090#1082#1088#1099#1090#1100' '#1090#1072#1073#1083#1080#1094#1091' '#1090#1072#1081#1083#1086#1074'...'
        ShortCut = 16450
        OnClick = ottnlClick
      end
      object rttbl: TMenuItem
        Caption = #1055#1077#1088#1077#1079#1072#1075#1088#1091#1079#1080#1090#1100' '#1090#1072#1073#1083#1080#1094#1091' '#1090#1072#1081#1083#1086#1074
        Enabled = False
        ShortCut = 122
        OnClick = rttblClick
      end
      object cttbl: TMenuItem
        Caption = #1047#1072#1082#1088#1099#1090#1100' '#1090#1072#1073#1083#1080#1094#1091' '#1090#1072#1081#1083#1086#1074
        Enabled = False
        ShortCut = 16459
        OnClick = cttblClick
      end
    end
    object TileMapMenu: TMenuItem
      Caption = #1050#1072#1088#1090#1072' '#1090#1072#1081#1083#1086#1074
      Enabled = False
      object GoTo1: TMenuItem
        Caption = #1055#1077#1088#1077#1081#1090#1080'...'
        ShortCut = 16455
        OnClick = GoTo1Click
      end
      object N18: TMenuItem
        Caption = '-'
      end
      object N4: TMenuItem
        Caption = #1057#1076#1074#1080#1085#1091#1090#1100' '#1085#1072' '#1090#1072#1081#1083' '#1074#1083#1077#1074#1086
        ShortCut = 8310
        OnClick = N4Click
      end
      object N5: TMenuItem
        Caption = #1057#1076#1074#1080#1085#1091#1090#1100' '#1085#1072' '#1090#1072#1081#1083' '#1074#1087#1088#1072#1074#1086
        ShortCut = 8311
        OnClick = N5Click
      end
      object N6: TMenuItem
        Caption = #1057#1076#1074#1080#1085#1091#1090#1100' '#1085#1072' '#1073#1072#1081#1090' '#1074#1083#1077#1074#1086
        ShortCut = 8314
        OnClick = N6Click
      end
      object N7: TMenuItem
        Caption = #1057#1076#1074#1080#1085#1091#1090#1100' '#1085#1072' '#1073#1072#1081#1090' '#1074#1087#1088#1072#1074#1086
        ShortCut = 8315
        OnClick = N7Click
      end
      object N8: TMenuItem
        Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1090#1072#1081#1083
        ShortCut = 8238
        OnClick = N8Click
      end
      object N22: TMenuItem
        Caption = '-'
      end
      object BMP1: TMenuItem
        Caption = #1048#1084#1087#1086#1088#1090' '#1080#1079' BMP...'
        ShortCut = 16457
        OnClick = BMP1Click
      end
      object BMP2: TMenuItem
        Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' BMP...'
        ShortCut = 16453
        OnClick = BMP2Click
      end
    end
    object DataMapMenu: TMenuItem
      Caption = #1044#1072#1085#1085#1099#1077
      Enabled = False
      object GoTo2: TMenuItem
        Caption = #1055#1077#1088#1077#1081#1090#1080'...'
        ShortCut = 16468
        OnClick = GoTo2Click
      end
      object SearchItem: TMenuItem
        Caption = #1055#1086#1080#1089#1082'...'
        ShortCut = 16454
        OnClick = SearchItemClick
      end
      object N25: TMenuItem
        Caption = #1055#1086#1089#1084#1086#1090#1088#1077#1090#1100' '#1088#1077#1079#1091#1083#1100#1090#1072#1090#1099' '#1087#1086#1080#1089#1082#1072'...'
        ShortCut = 16506
        OnClick = N25Click
      end
      object N19: TMenuItem
        Caption = '-'
      end
      object N9: TMenuItem
        Caption = #1057#1076#1074#1080#1085#1091#1090#1100' '#1085#1072' '#1073#1072#1081#1090' '#1074#1083#1077#1074#1086
        ShortCut = 8308
        OnClick = N9Click
      end
      object N10: TMenuItem
        Caption = #1057#1076#1074#1080#1085#1091#1090#1100' '#1085#1072' '#1073#1072#1081#1090' '#1074#1087#1088#1072#1074#1086
        ShortCut = 8309
        OnClick = N10Click
      end
      object N28: TMenuItem
        Caption = #1054#1095#1080#1089#1090#1080#1090#1100
        ShortCut = 16452
        OnClick = N28Click
      end
      object N21: TMenuItem
        Caption = '-'
      end
      object N20: TMenuItem
        Caption = #1048#1079#1074#1083#1077#1095#1100' '#1090#1077#1082#1089#1090'...'
        Enabled = False
        ShortCut = 115
        OnClick = N20Click
      end
      object N24: TMenuItem
        Caption = #1047#1072#1084#1077#1085#1080#1090#1100' '#1090#1077#1082#1089#1090'...'
        Enabled = False
        ShortCut = 16499
        OnClick = N24Click
      end
      object Getstring1: TMenuItem
        Caption = #1048#1079#1074#1083#1077#1095#1100' '#1089#1090#1088#1086#1082#1091
        Enabled = False
        ShortCut = 116
        OnClick = Getstring1Click
      end
      object PutString1: TMenuItem
        Caption = #1042#1089#1090#1072#1074#1080#1090#1100' '#1089#1090#1088#1086#1082#1091
        Enabled = False
        ShortCut = 45
        OnClick = PutString1Click
      end
    end
    object PaletteMenu: TMenuItem
      Caption = #1055#1072#1083#1080#1090#1088#1072
      object Restore1: TMenuItem
        Caption = #1042#1086#1089#1089#1090#1072#1085#1086#1074#1080#1090#1100
        ShortCut = 16466
        OnClick = Restore1Click
      end
      object N1: TMenuItem
        Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100'...'
        ShortCut = 16460
        OnClick = N1Click
      end
      object N2: TMenuItem
        Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100'...'
        ShortCut = 16470
        OnClick = N2Click
      end
    end
    object optItm: TMenuItem
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
      OnClick = optItmClick
      object LCnt: TMenuItem
        Caption = #1055#1086#1089#1080#1084#1074#1086#1083#1100#1085#1099#1081' '#1087#1086#1076#1089#1095#1077#1090' '#1076#1083#1080#1085#1099' '#1089#1090#1088#1086#1082#1080
        ShortCut = 120
        OnClick = LCntClick
      end
      object N14: TMenuItem
        Caption = '-'
      end
      object N11: TMenuItem
        Caption = #1050#1072#1088#1090#1072' '#1090#1072#1081#1083#1086#1074
        Checked = True
        ShortCut = 49217
        OnClick = N11Click
      end
      object N12: TMenuItem
        Caption = #1056#1077#1076#1072#1082#1090#1086#1088' '#1074#1099#1073#1088#1072#1085#1085#1086#1075#1086' '#1090#1072#1081#1083#1072
        Checked = True
        ShortCut = 49218
        OnClick = N12Click
      end
      object N13: TMenuItem
        Caption = #1058#1077#1082#1089#1090', '#1082#1086#1076' '#1080' '#1087#1088#1086#1095#1080#1081' '#1084#1091#1089#1086#1088
        Checked = True
        ShortCut = 49219
        OnClick = N13Click
      end
      object N23: TMenuItem
        Caption = '-'
      end
      object vstrfont: TMenuItem
        Caption = #1048#1089#1087#1086#1083#1100#1079#1086#1074#1072#1090#1100' '#1074#1089#1090#1088#1086#1077#1085#1085#1099#1081' '#1096#1088#1080#1092#1090
        ShortCut = 16471
        OnClick = vstrfontClick
      end
      object N26: TMenuItem
        Caption = '-'
      end
      object N27: TMenuItem
        Caption = #1056#1072#1089#1087#1086#1083#1086#1078#1080#1090#1100' '#1086#1082#1085#1072' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
        ShortCut = 16507
        OnClick = N27Click
      end
    end
    object HelpMenu: TMenuItem
      Caption = #1055#1086#1084#1086#1097#1100
      object N3: TMenuItem
        Caption = #1060#1086#1088#1084#1072#1090#1099' '#1090#1072#1073#1083#1080#1094'...'
        ShortCut = 8304
        OnClick = N3Click
      end
      object AboutItem: TMenuItem
        Caption = #1054' '#1087#1088#1086#1075#1088#1072#1084#1084#1077'...'
        ShortCut = 112
        OnClick = AboutItemClick
      end
    end
  end
  object ColorDialog: TColorDialog
    Ctl3D = True
    CustomColors.Strings = (
      'ColorA=FFFFFFFF'
      'ColorB=FFFFFFFF'
      'ColorC=FFFFFFFF'
      'ColorD=FFFFFFFF'
      'ColorE=FFFFFFFF'
      'ColorF=FFFFFFFF'
      'ColorG=FFFFFFFF'
      'ColorH=FFFFFFFF'
      'ColorI=FFFFFFFF'
      'ColorJ=FFFFFFFF'
      'ColorK=FFFFFFFF'
      'ColorL=FFFFFFFF'
      'ColorM=FFFFFFFF'
      'ColorN=FFFFFFFF'
      'ColorO=FFFFFFFF'
      'ColorP=FFFFFFFF')
    Options = [cdFullOpen, cdPreventFullOpen, cdSolidColor, cdAnyColor]
    Left = 328
    Top = 48
  end
  object MakeTBL: TSaveDialog
    DefaultExt = '.tbl'
    Filter = #1058#1072#1073#1083#1080#1094#1072' (*.tbl)|*.tbl|'#1042#1089#1077' '#1092#1072#1081#1083#1099' (*.*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofExtensionDifferent, ofEnableSizing]
    Title = #1057#1086#1079#1076#1072#1085#1080#1077' '#1096#1072#1073#1083#1086#1085#1072' '#1090#1072#1073#1083#1080#1094#1080
    Left = 264
    Top = 48
  end
  object OpenTBL: TOpenDialog
    DefaultExt = '.tbl'
    Filter = #1058#1072#1073#1083#1080#1094#1072' (*.tbl)|*.tbl|'#1042#1089#1077' '#1092#1072#1081#1083#1099' (*.*)|*.*'
    Options = [ofHideReadOnly, ofExtensionDifferent, ofEnableSizing]
    Title = #1047#1072#1075#1088#1091#1079#1082#1072' '#1090#1072#1073#1083#1080#1094#1099
    Left = 168
    Top = 48
  end
  object SavePAL: TSaveDialog
    DefaultExt = '.pal'
    Filter = #1055#1072#1083#1080#1090#1088#1072' (*.pal)|*.pal|'#1042#1089#1077' '#1092#1072#1081#1083#1099' (*.*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofExtensionDifferent, ofEnableSizing]
    Title = #1057#1086#1093#1088#1072#1085#1077#1085#1080#1077' '#1087#1072#1083#1080#1090#1088#1099
    Left = 232
    Top = 48
  end
  object OpenPAL: TOpenDialog
    DefaultExt = '.pal'
    Filter = #1055#1072#1083#1080#1090#1088#1072' (*.pal)|*.pal|'#1042#1089#1077' '#1092#1072#1081#1083#1099' (*.*)|*.*'
    Options = [ofHideReadOnly, ofExtensionDifferent, ofEnableSizing]
    Title = #1047#1072#1075#1088#1091#1079#1082#1072' '#1087#1072#1083#1080#1090#1088#1099
    Left = 136
    Top = 48
  end
end
