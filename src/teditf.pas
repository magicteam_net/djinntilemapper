unit teditf;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, ExtCtrls;

type
  Tteditform = class(TForm)
    geditpan: TPanel;
    Tile: TImage;
    ClearBtn: TSpeedButton;
    RotRightBtn: TSpeedButton;
    RotLeftBtn: TSpeedButton;
    Flip2Btn: TSpeedButton;
    FlipBtn: TSpeedButton;
    MvUpBtn: TSpeedButton;
    MvRightBtn: TSpeedButton;
    MvLeftBtn: TSpeedButton;
    MvDownBtn: TSpeedButton;
    Color1im: TImage;
    Color2im: TImage;
    Color3im: TImage;
    Color4im: TImage;
    c16im: TImage;
    procedure MvLeftBtnClick(Sender: TObject);
    procedure MvRightBtnClick(Sender: TObject);
    procedure MvUpBtnClick(Sender: TObject);
    procedure MvDownBtnClick(Sender: TObject);
    procedure FlipBtnClick(Sender: TObject);
    procedure Flip2BtnClick(Sender: TObject);
    procedure RotLeftBtnClick(Sender: TObject);
    procedure RotRightBtnClick(Sender: TObject);
    procedure ClearBtnClick(Sender: TObject);
    procedure c16imMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Color1imClick(Sender: TObject);
    procedure Color1imContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure Color1imDblClick(Sender: TObject);
    procedure TileMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure TileMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure TileMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  teditform: Tteditform;

implementation

{$R *.dfm}

uses dtmmain, tmform;

procedure Tteditform.MvLeftBtnClick(Sender: TObject);
Var b: Array[0..8 * 32 - 1, 0..8 * 32 - 1] of TColor; xx, yy: Byte;
begin
 If RomOpened then
 begin
  If not b16x16 then
  begin
   For yy := 0 to 7 do
    For xx := 0 to 7 do
     b[xx, yy] := bTile.Canvas.Pixels[xx, yy];
   tx := 7;
   For yy := 0 to 7 do
   begin
    ty := yy;
    bTile.Canvas.Pixels[7, yy] := b[0, yy];
    ddd;
   end;
   For yy := 0 to 7 do
    For xx := 0 to 6 do
    begin
     tx := xx; ty := yy;
     bTile.Canvas.Pixels[xx, yy] := b[xx + 1, yy];
     ddd;
    end;
  end Else
  begin
   For yy := 0 to 15 do
    For xx := 0 to 15 do
     b[xx, yy] := bTile.Canvas.Pixels[xx, yy];
   tx := 15;
   For yy := 0 to 15 do
   begin
    ty := yy;
    bTile.Canvas.Pixels[15, yy] := b[0, yy];
    ddd;
   end;
   For yy := 0 to 15 do
    For xx := 0 to 14 do
    begin
     tx := xx; ty := yy;
     bTile.Canvas.Pixels[xx, yy] := b[xx + 1, yy];
     ddd;
    end;
  end;
  Saved := False;
  djinntilemapper.ROMPos := DataPos;
 end;
end;


procedure Tteditform.MvRightBtnClick(Sender: TObject);
Var b: Array[0..8 * 32 - 1, 0..8 * 32 - 1] of TColor; xx, yy: Byte;
begin
 If RomOpened then
 begin
  If not b16x16 then
  begin
   For yy := 0 to 7 do
    For xx := 0 to 7 do
     b[xx, yy] := bTile.Canvas.Pixels[xx, yy];
   tx := 0;
   For yy := 0 to 7 do
   begin
    ty := yy;
    bTile.Canvas.Pixels[0, yy] := b[7, yy];
    ddd;
   end;
   For yy := 0 to 7 do
    For xx := 1 to 7 do
    begin
     tx := xx; ty := yy;
     bTile.Canvas.Pixels[xx, yy] := b[xx - 1, yy];
     ddd;
    end;
  end Else
  begin
   For yy := 0 to 15 do
    For xx := 0 to 15 do
     b[xx, yy] := bTile.Canvas.Pixels[xx, yy];
   tx := 0;
   For yy := 0 to 15 do
   begin
    ty := yy;
    bTile.Canvas.Pixels[0, yy] := b[15, yy];
    ddd;
   end;
   For yy := 0 to 15 do
    For xx := 1 to 15 do
    begin
     tx := xx; ty := yy;
     bTile.Canvas.Pixels[xx, yy] := b[xx - 1, yy];
     ddd;
    end;
  end;
  Saved := False;
  djinntilemapper.ROMPos := DataPos;
 end;
end;


procedure Tteditform.MvUpBtnClick(Sender: TObject);
Var b: Array[0..8 * 32 - 1, 0..8 * 32 - 1] of TColor; xx, yy: Byte;
begin
 If RomOpened then
 begin
  if not b16x16 then
  begin
   For yy := 0 to 7 do
    For xx := 0 to 7 do
     b[xx, yy] := bTile.Canvas.Pixels[xx, yy];
   ty := 7;
   For xx := 0 to 7 do
   begin
    tx := xx;
    bTile.Canvas.Pixels[xx, 7] := b[xx, 0];
    ddd;
   end;
   For yy := 0 to 6 do
    For xx := 0 to 7 do
    begin
     tx := xx; ty := yy;
     bTile.Canvas.Pixels[xx, yy] := b[xx, yy + 1];
     ddd;
    end;
  end Else
  begin
   For yy := 0 to 15 do
    For xx := 0 to 15 do
     b[xx, yy] := bTile.Canvas.Pixels[xx, yy];
   ty := 15;
   For xx := 0 to 15 do
   begin
    tx := xx;
    bTile.Canvas.Pixels[xx, 15] := b[xx, 0];
    ddd;
   end;
   For yy := 0 to 14 do
    For xx := 0 to 15 do
    begin
     tx := xx; ty := yy;
     bTile.Canvas.Pixels[xx, yy] := b[xx, yy + 1];
     ddd;
    end;
  end;
  Saved := False;
  djinntilemapper.ROMPos := DataPos;
 end;
end;


procedure Tteditform.MvDownBtnClick(Sender: TObject);
Var b: Array[0..8 * 32 - 1, 0..8 * 32 - 1] of TColor; xx, yy: Byte;
begin
 If RomOpened then
 begin
  If not b16x16 then
  begin
   For yy := 0 to 7 do
    For xx := 0 to 7 do
     b[xx, yy] := bTile.Canvas.Pixels[xx, yy];
   ty := 0;
   For xx := 0 to 7 do
   begin
    tx := xx;
    bTile.Canvas.Pixels[xx, 0] := b[xx, 7];
    ddd;
   end;
   For yy := 1 to 7 do
    For xx := 0 to 7 do
    begin
     tx := xx; ty := yy;
     bTile.Canvas.Pixels[xx, yy] := b[xx, yy - 1];
     ddd;
    end;
  end Else
  begin
   For yy := 0 to 15 do
    For xx := 0 to 15 do
     b[xx, yy] := bTile.Canvas.Pixels[xx, yy];
   ty := 0;
   For xx := 0 to 15 do
   begin
    tx := xx;
    bTile.Canvas.Pixels[xx, 0] := b[xx, 15];
    ddd;
   end;
   For yy := 1 to 15 do
    For xx := 0 to 15 do
    begin
     tx := xx; ty := yy;
     bTile.Canvas.Pixels[xx, yy] := b[xx, yy - 1];
     ddd;
    end;
  end;
  Saved := False;
  djinntilemapper.ROMPos := DataPos;
 end;
end;
var txx, tyy: integer;

procedure Tteditform.FlipBtnClick(Sender: TObject);
Var b: Array[0..8 * 32 - 1, 0..8 * 32 - 1] of TColor; xx, yy: Byte;
begin
 If RomOpened then
 begin
  If not b16x16 then
  begin
   For yy := 0 to 7 do
    For xx := 0 to 7 do
     b[xx, yy] := bTile.Canvas.Pixels[xx, yy];
   For Tyy := 0 to 7 do
    For Txx := 0 to 7 do
    begin
     ty := tyy; tx := txx;
     bTile.Canvas.Pixels[Tx, Ty] := b[7 - Tx, Ty];
     ddd;
    end;
  end Else
  begin
   For yy := 0 to 15 do
    For xx := 0 to 15 do
     b[xx, yy] := bTile.Canvas.Pixels[xx, yy];
   For Tyy := 0 to 15 do
    For Txx := 0 to 15 do
    begin
     ty := tyy; tx := txx;
     bTile.Canvas.Pixels[Tx, Ty] := b[15 - Tx, Ty];
     ddd;
    end;
  end;
  Saved := False;
  djinntilemapper.ROMPos := DataPos;
 end;
end;

procedure Tteditform.Flip2BtnClick(Sender: TObject);
Var b: Array[0..8 * 32 - 1, 0..8 * 32 - 1] of TColor; xx, yy: Byte;
begin
 If RomOpened then
 begin
  If not b16x16 then
  begin
   For yy := 0 to 7 do
    For xx := 0 to 7 do
     b[xx, yy] := bTile.Canvas.Pixels[xx, yy];
   For Tyy := 0 to 7 do
    For Txx := 0 to 7 do
    begin
     ty := tyy; tx := txx;    
     bTile.Canvas.Pixels[Tx, Ty] := b[Tx, 7 - Ty];
     ddd;
    end;
  end else
  begin
   For yy := 0 to 15 do
    For xx := 0 to 15 do
     b[xx, yy] := bTile.Canvas.Pixels[xx, yy];
   For Tyy := 0 to 15 do
    For Txx := 0 to 15 do
    begin
     ty := tyy; tx := txx;    
     bTile.Canvas.Pixels[Tx, Ty] := b[Tx, 15 - Ty];
     ddd;
    end;
  end;
  Saved := False;
  djinntilemapper.ROMPos := DataPos;
 end;
end;


procedure Tteditform.RotLeftBtnClick(Sender: TObject);
Var b: Array[0..8 * 32 - 1, 0..8 * 32 - 1] of TColor; xx, yy: Byte;
begin
 If RomOpened then
 begin
  If not b16x16 then
  begin
   For yy := 0 to 7 do
    For xx := 0 to 7 do
     b[xx, yy] := bTile.Canvas.Pixels[xx, yy];
   For Tyy := 0 to 7 do
    For Txx := 0 to 7 do
    begin
     ty := tyy; tx := txx;    
     bTile.Canvas.Pixels[Tx, Ty] := b[7 - Ty, Tx];
     ddd;
    end;
  end else
  begin
   For yy := 0 to 15 do
    For xx := 0 to 15 do
     b[xx, yy] := bTile.Canvas.Pixels[xx, yy];
   For Tyy := 0 to 15 do
    For Txx := 0 to 15 do
    begin
     ty := tyy; tx := txx;    
     bTile.Canvas.Pixels[Tx, Ty] := b[15 - Ty, Tx];
     ddd;
    end;
  end;
  Saved := False;
  djinntilemapper.ROMPos := DataPos;
 end;
end;


procedure Tteditform.RotRightBtnClick(Sender: TObject);
Var b: Array[0..8 * 32 - 1, 0..8 * 32 - 1] of TColor; xx, yy: Byte;
begin
 If RomOpened then
 begin
  if not b16x16 then
  begin
   For yy := 0 to 7 do
    For xx := 0 to 7 do
     b[xx, yy] := bTile.Canvas.Pixels[xx, yy];
   For Tyy := 0 to 7 do
    For Txx := 0 to 7 do
    begin
     ty := tyy; tx := txx;    
     bTile.Canvas.Pixels[Tx, Ty] := b[Ty, 7 - Tx];
     ddd;
    end;
  end else
  begin
   For yy := 0 to 15 do
    For xx := 0 to 15 do
     b[xx, yy] := bTile.Canvas.Pixels[xx, yy];
   For Tyy := 0 to 15 do
    For Txx := 0 to 15 do
    begin
     ty := tyy; tx := txx;    
     bTile.Canvas.Pixels[Tx, Ty] := b[Ty, 15 - Tx];
     ddd;
    end;
  end;
  Saved := False;
  djinntilemapper.ROMPos := DataPos;
 end;
end;


procedure Tteditform.ClearBtnClick(Sender: TObject);
begin
 If RomOpened then
 begin
  If djinntilemapper.codec < cdc4bppsnes then
  begin
   If b16x16 then For Tyy := 0 to 15 do
    For Txx := 0 to 15 do
    begin
     ty := tyy; tx := txx;    
     bTile.Canvas.Pixels[Tx, Ty] := Colors[BCol];
     ddd;
    end else
   For Tyy := 0 to 7 do
    For Txx := 0 to 7 do
    begin
     ty := tyy; tx := txx;    
     bTile.Canvas.Pixels[Tx, Ty] := Colors[BCol];
     ddd;
    end;
  end Else
   For Tyy := 0 to 7 do
    For Txx := 0 to 7 do
    begin
     ty := tyy; tx := txx;    
     bTile.Canvas.Pixels[Tx, Ty] := Col16[BCol];
     ddd;
    end;
  Saved := False;
  djinntilemapper.ROMPos := DataPos;
 end;
end;

procedure Tteditform.c16imMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
 If button = mbleft then
 begin
  if ssdouble in shift then
  begin
   djinntilemapper.colordialog.Color := col16[y div 16];
   If djinntilemapper.Colordialog.Execute then
   begin
    col16[y div 16] := djinntilemapper.colordialog.Color;
    if ROMopened then djinntilemapper.DrawTileMap;
   end;
  end;
  djinntilemapper.Foreground := y div 16;
 end
 Else
  djinntilemapper.Background := y div 16
end;

procedure Tteditform.Color1imClick(Sender: TObject);
begin
 djinntilemapper.Foreground := Timage(Sender).Tag;
end;

procedure Tteditform.Color1imContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
begin
 djinntilemapper.Background := Timage(Sender).Tag;
end;

procedure Tteditform.Color1imDblClick(Sender: TObject);
begin
 Case TImage(Sender).Tag of
  1: djinntilemapper.colordialog.color := djinntilemapper.Color1;
  2: djinntilemapper.colordialog.color := djinntilemapper.Color2;
  3: djinntilemapper.colordialog.color := djinntilemapper.Color3;
  4: djinntilemapper.colordialog.color := djinntilemapper.Color4;
 end;
 If djinntilemapper.Colordialog.Execute then
 Case TImage(Sender).Tag of
  1: djinntilemapper.Color1 := djinntilemapper.colordialog.color;
  2: djinntilemapper.Color2 := djinntilemapper.colordialog.color;
  3: djinntilemapper.Color3 := djinntilemapper.colordialog.color;
  4: djinntilemapper.Color4 := djinntilemapper.colordialog.color;
 end;
 If ROMopened then djinntilemapper.DrawTileMap;
end;

procedure Tteditform.TileMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
 Drawing := True;
 If ROMOpened then
 begin
  If Boolean(Button) then Col := BCol Else Col := FCol;
  If djinntilemapper.codec < cdc4bppsnes then
  begin
   bTile.Canvas.Pixels[tx, ty] := Colors[Col];
   caption := inttostr(bTile.Canvas.Pixels[tx, ty]);   
  end
  else
  begin
   bTile.Canvas.Pixels[tx, ty] := Col16[Col];
  end;
  Tile.Canvas.StretchDraw(Bounds(0, 0, 256, 256), bTile);
  ddd;
 end;
end;

procedure Tteditform.TileMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
 If not b16x16 and (tilew <= 8) and (tileh <= 8) then
 begin
   tx :=  x div 32;
   ty :=  y div 32;
  mtx := tx  *  32;
  mty := ty  *  32;
 end Else
 begin
   tx :=  x div 16;
   ty :=  y div 16;
  mtx := tx  *  16;
  mty := ty  *  16;
 end;
 If ROMOpened and ((ssRight in Shift) or (ssLeft in Shift)) then
 begin
  Drawing := True;
  If ssRight in Shift then Col := BCol Else if ssLeft in shift then Col := FCol;
  If djinntilemapper.codec < cdc4bppsnes then
   bTile.Canvas.Pixels[tx, ty] := Colors[Col]
  else
   bTile.Canvas.Pixels[tx, ty] := Col16[Col];
  Tile.Canvas.StretchDraw(Bounds(0, 0, 256, 256), bTile);
  ddd;
 end;
end;

procedure Tteditform.TileMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
 Drawing := False;
 If ROmopened then
 begin
  Saved := False;
  djinntilemapper.ROMPos := DataPos;
 end;
end;

end.
