unit DTMmain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus, ColorGrd, StdCtrls, ComCtrls, Gauges, ToolWin,
  ImgList, Buttons, Spin, InputBox;

Type
 TCodec =
 (cdc1BPP, cdc2BPPNES, cdc2BPPGB, cdc4BPPSNES, cdc4BPPSNESm,
  cdc4BPPGBA, cdc4BPPMSX, cdc4BPPSMS);

type
  TDjinnTileMapper = class(TForm)
    OpenROM: TOpenDialog;
    SaveROM: TSaveDialog;
    MainMenu: TMainMenu;
    FileMenu: TMenuItem;
    TileMapMenu: TMenuItem;
    PaletteMenu: TMenuItem;
    HelpMenu: TMenuItem;
    OpenItem: TMenuItem;
    ReloadItem: TMenuItem;
    SaveItem: TMenuItem;
    SaveASitem: TMenuItem;
    ExitItem: TMenuItem;
    ColorDialog: TColorDialog;
    GoTo1: TMenuItem;
    DataMapMenu: TMenuItem;
    GoTo2: TMenuItem;
    Restore1: TMenuItem;
    TableMenu: TMenuItem;
    MkTmplItem: TMenuItem;
    OpenTblItem: TMenuItem;
    AboutItem: TMenuItem;
    MakeTBL: TSaveDialog;
    OpenTBL: TOpenDialog;
    TBLnamel: TLabel;
    Rldtblitem: TMenuItem;
    DataEdit: TEdit;
    LenLab: TLabel;
    MaxLenLab: TLabel;
    PasteBtn: TButton;
    GetBtn: TButton;
    Getstring1: TMenuItem;
    PutString1: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    SavePAL: TSaveDialog;
    OpenPAL: TOpenDialog;
    SearchItem: TMenuItem;
    SpinEdit1: TSpinEdit;
    ottnl: TMenuItem;
    rttbl: TMenuItem;
    cttbl: TMenuItem;
    Label4: TLabel;
    N3: TMenuItem;
    LCnt: TMenuItem;
    optItm: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    N18: TMenuItem;
    N19: TMenuItem;
    N20: TMenuItem;
    N21: TMenuItem;
    N22: TMenuItem;
    BMP1: TMenuItem;
    BMP2: TMenuItem;
    N23: TMenuItem;
    vstrfont: TMenuItem;
    N24: TMenuItem;
    N25: TMenuItem;
    N26: TMenuItem;
    N27: TMenuItem;
    N28: TMenuItem;
    procedure FormShow(Sender: TObject);
    procedure ExitItemClick(Sender: TObject);
    Procedure Fill(Var Im: TImage; Clr: TColor);
    procedure OpenItemClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure GoTo1Click(Sender: TObject);
    procedure GoTo2Click(Sender: TObject);
    procedure ReloadItemClick(Sender: TObject);
    procedure SaveItemClick(Sender: TObject);
    procedure SaveASitemClick(Sender: TObject);
    procedure Restore1Click(Sender: TObject);
    procedure MkTmplItemClick(Sender: TObject);
    procedure OpenTblItemClick(Sender: TObject);
    procedure DataEditKeyPress(Sender: TObject; var Key: Char);
    procedure DataEditChange(Sender: TObject);
    procedure PasteBtnClick(Sender: TObject);
    procedure RldtblitemClick(Sender: TObject);
    procedure GetBtnClick(Sender: TObject);
    procedure Getstring1Click(Sender: TObject);
    procedure PutString1Click(Sender: TObject);
    procedure AboutItemClick(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure SearchItemClick(Sender: TObject);
    procedure SpinEdit1Change(Sender: TObject);
    procedure ottnlClick(Sender: TObject);
    procedure cttblClick(Sender: TObject);
    procedure rttblClick(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure LCntClick(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure N10Click(Sender: TObject);
    procedure tmpanDockOver(Sender: TObject; Source: TDragDockObject; X,
      Y: Integer; State: TDragState; var Accept: Boolean);
    procedure N11Click(Sender: TObject);
    procedure N12Click(Sender: TObject);
    procedure N13Click(Sender: TObject);
    procedure optItmClick(Sender: TObject);
    procedure BMP1Click(Sender: TObject);
    procedure BMP2Click(Sender: TObject);
    procedure vstrfontClick(Sender: TObject);
    procedure N20Click(Sender: TObject);
    procedure N24Click(Sender: TObject);
    procedure N25Click(Sender: TObject);
    procedure N27Click(Sender: TObject);
    procedure N28Click(Sender: TObject);
  protected
    Procedure SetCol1(Color: TColor);
    Function  GetCol1: TColor;
    Procedure SetCol2(Color: TColor);
    Function  GetCol2: TColor;
    Procedure SetCol3(Color: TColor);
    Function  GetCol3: TColor;
    Procedure SetCol4(Color: TColor);
    Function  GetCol4: TColor;
    Procedure SetF(n: byte);
    Function  GetF: byte;
    Procedure SetB(n: byte);
    Function  GetB: byte;
    Procedure SetCodec(c: TCodec);
    Function  GetCodec: TCodec;
    Procedure SetPos(p: LongInt);
    Function  GetPos: LongInt;
    Procedure SetrPos(p: LongInt);
    Function  GetrPos: LongInt;
    Procedure SetCurbank(n: integer);
    Function  GetCurbank: integer;
    Procedure Setdwidth(w: integer);
    Function  Getdwidth: integer;
    Procedure SetdHeight(h: integer);
    Function  GetdHeight: integer;
    Procedure Setdbank(n: integer);
    Function  Getdbank: integer;
    Procedure Setcol(i: integer; n: TColor);
    Function  Getcol(i: integer): TColor;
    Procedure Set16x16(t: boolean);
    Function  Get16x16: boolean;

  public
    procedure initposes;
    Function GetLength: Integer;
    procedure InitDataEdit;
    Procedure ROMopen;
    Procedure DrawTile;
    Procedure DrawTileMap;
    Procedure MapScrollEnable;
    Procedure DataMapDraw;
    Procedure DataScrollEnable;
    procedure srelative; 
    Property Color1: TColor Read GetCol1 Write SetCol1;
    Property Color2: TColor Read GetCol2 Write SetCol2;
    Property Color3: TColor Read GetCol3 Write SetCol3;
    Property Color4: TColor Read GetCol4 Write SetCol4;
    Property Colors16[i: Integer]: TColor Read Getcol Write Setcol;
    Property Foreground: byte Read GetF Write SetF;
    Property Background: byte Read GetB Write SetB;
    Property t16x16: Boolean Read Get16x16 Write Set16x16;

    Property Codec: TCodec Read GetCodec Write SetCodec;
    Property ROMpos: LongInt Read getPos Write SetPos;
    Property rDatapos: LongInt Read getrPos Write SetrPos;
    Property Bank: integer Read getcurbank Write Setcurbank;
    Property dWidth: Integer Read getdwidth Write Setdwidth;
    Property dHeight: Integer Read getdHeight Write SetdHeight;
    Property DataBank: Integer Read getdbank Write Setdbank;
  end;

Procedure ddd;
Procedure ddd16;

var
  DjinnTileMapper: TDjinnTileMapper;

Type
 PromData = ^TromData;
 TromData = Array[0..10 * 1024 * 1024] of Byte;
 TTable = Array[Byte] of string;
 TTileTable = Array[Byte] of Byte;

Const Hchs: TCharset = ['H', 'h', '0'..'9', 'A'..'F', 'a'..'f', #8];
Const errs: String = '����������� ������� �����!!!';

var
  perenos, endt: byte;
  tblname: string;
  tblopened: boolean;
  ttblname: string;
  ttblopened: boolean;
  Table: TTable;
  TileTable: TTileTable;
  shiftprsd: boolean;
  DEditSet: set of Char;
  maxtlen: byte;
  maxSlen: iNTEGER;
  tlwidthv: integer;
  tlheightv: integer;

Function HexVal(s: String; Var err: Boolean): LongInt;

Const
 //Capt: String; = Application.Title;//'Djinn Tile Mapper v1.4.9.9-beta';
 Ss: Array[Boolean] of String[1] = ('*', '');
 bsz: Array[TCodec] of Byte = (1, 2, 2, 4, 4, 4, 4, 4);
Var
 Colors: Array[1..4] of TColor;
 col16: Array[0..15] of TColor;
 FCol, BCol: Byte;
 fgi, bgi: ^TImage;
 ROM: file;
 fname: string;
 ROMData: PROMdata = NIL;
 ROMSize: LongInt;
 ROMopened: Boolean;
 Cdc: TCodec; bmap, btile: tbitmap;
 DataPos, romDataPos: LongInt; Curbank: Integer;
 tmMouseX, tmMouseY: Integer; dataw, datah: Integer;
 dmMouseX, dmMouseY: Integer; CDPos: Integer;
 Saved: Boolean; Drawing: Boolean;
 col: Byte; tx, ty, mtx, mty: Integer;
 b16x16: boolean;
  tilew: byte;
  tileh: byte;

implementation

uses About, Search, crtrd, srres, tmform, teditf, dataf;

{$R *.dfm}


Procedure TDjinnTileMapper.Set16x16(t: boolean);
begin
 b16x16 := t;
 If t then
 begin
  bmap.Width := 256;
  bmap.Height := 256;
  btile.Width := 16;
  btile.Height := 16;
 end Else
 begin
  bmap.Width := tilew * 16;
  bmap.Height := tileh * 16;
  if (tilew > 8) or (tileh > 8) then
  begin
   btile.Width := 16;
   btile.Height := 16;
  end Else
  begin
   btile.Width := 8;
   btile.Height := 8;
  end;
 end;
 If romopened then
 begin
  MapScrollEnable;
  drawtilemap;
 end;
end;

Function  TDjinnTileMapper.Get16x16: boolean;
begin
 Result := b16x16;
end;

Procedure TDjinnTileMapper.DataMapDraw;
var p: PROMdata; xx, yy: integer; tile: byte; bbb: tbitmap;
begin
 bbb := TBitmap.Create;
 bbb.width := dataform.datamap.Width;
 bbb.Height := dataform.datamap.Height;
 bbb.Canvas.Font.Color := $D0D0D0;
 bbb.Canvas.Font.style := [fsbold];
 bbb.Canvas.Brush.Color := 0;

 bbb.Canvas.FillRect(bounds(0, 0, bbb.width, bbb.height));
 p := Addr(ROMdata^[romDatapos]);
 For yy := 0 to DHeight - 1 do
  For xx := 0 to DWidth - 1 do
  begin
   If not tTblopened then Tile := p^[yy * DWidth + xx] Else
                          Tile := TileTable[p^[yy * DWidth + xx]];
   If vstrfont.Checked then
   begin
    If not tblopened then
     bbb.Canvas.TextOut(xx shl 4, yy shl 4, char(tile)) Else
     bbb.Canvas.TextOut(xx shl 4, yy shl 4, table[tile][1]);
   end
   Else
   bbb.Canvas.CopyRect(Bounds(xx shl 4, yy shl 4, 16, 16), bmap.Canvas,
            Bounds((Tile mod 16) shl (3 + Byte(b16x16)),
(Tile div 16) shl (3 + Byte(b16x16)), 8 + 8 * Byte(b16x16), 8 + 8 * Byte(b16x16)));
  end;
 dataform.dataMap.Canvas.Draw(0, 0, bbb);
 bbb.free;
 If CDPos >= dWidth * dHeight then CDPos := dWidth * dHeight - 1;
 dataform.Datamap.canvas.Rectangle(Bounds((CDpos mod dWidth) shl 4, (CDpos div dWidth) shl 4, 16, 16));
 dataform.Signl.Caption := '= ' + IntToStr(RomData^[romDatapos + CDPos]);
 dataform.HexEd.Tag := 0;
 dataform.hexed.Text := IntToHex(RomData^[romDatapos + CDPos], 2);
 dataform.HexEd.Tag := 1;
end;

Procedure TDjinnTileMapper.DrawTile;
begin
 bTile.Canvas.Brush.Color := 0;
 bTile.Canvas.FillRect(Bounds(0, 0, btile.Width, btile.Height));
 if b16x16 then
 teditform.caption := '���� #' + inttohex(curbank,2) + ' - ['
 + inttohex(datapos + longword(curbank) * (tilew * bsz[cdc] +
  24 * bsz[cdc] * Byte(b16x16)), 8) + ']' else
 teditform.caption := '���� #' + inttohex(curbank,2) + ' - ['
 + inttohex(datapos + Bank * ((tilew * tileh) div (8 div bsz[codec]) - 1 *
   byte((bank * tilew * tileh) mod (8 div bsz[codec]) > 0)) * bsz[Codec], 8) + ']';
 bTile.Canvas.CopyRect(Bounds(0, 0, btile.width, btile.height), bmap.Canvas,
            Bounds((Bank mod 16) * tilew,
            (Bank div 16) * tileh, btile.width, btile.height));
 fill(teditform.Tile, 0);
 if (btile.width <= 8) and (btile.height <= 8) then
  teditform.Tile.Canvas.CopyRect(Bounds(0, 0, tilew * 32, tileh * 32), bTile.Canvas,
             Bounds(0, 0, tilew, tileh)) Else
  teditform.Tile.Canvas.CopyRect(Bounds(0, 0, tilew * 16, tileh * 16), bTile.Canvas,
             Bounds(0, 0, tilew, tileh));
end;


Procedure TDjinnTileMapper.Setdwidth(w: integer);
begin
 If w < 1 then w := 1;
 If w > 32 then w := 32;
 dataform.Datamap.Width := 16 * w;
 dataform.Datamap.Left := (dataform.Panel1.Width - (dataform.datamap.Width + dataform.DataScroll.Width)) div 2;
 dataform.Datamap.Top := (dataform.Panel1.Height - dataform.datamap.Height) div 2;
 dataform.DataScroll.Left := dataform.Datamap.Left + dataform.Datamap.Width;
 dataw := w;
 If ROMopened then
 begin
  DataScrollEnable;
  DataMapDraw;
 end;
end;

Function  TDjinnTileMapper.Getdwidth: integer;
begin
{ dataw := WidthEdit.Value;
 If dataw < 1 then dataw := 1;
 If dataw > 32 then dataw := 32;}
 result := dataw;
end;

Procedure TDjinnTileMapper.SetdHeight(h: integer);
begin
 If h < 1 then h:= 1;
 If h > 32 then h := 32;
 dataform.Datamap.Height := 16 * h;
 dataform.Datamap.Left := (dataform.Panel1.Width - (dataform.datamap.Width + dataform.DataScroll.Width)) div 2;
 dataform.Datamap.Top := (dataform.Panel1.Height - dataform.datamap.Height) div 2;
 dataform.DataScroll.Top := dataform.Datamap.Top;
 dataform.dataScroll.Height := dataform.Datamap.Height;
 datah := h;
 If ROMopened then
 begin
  DataScrollEnable;
  DataMapDraw;
 end;
end;

Function  TDjinnTileMapper.GetdHeight: integer;
begin
{ datah := HeightEdit.Value;
 If datah < 1 then datah := 1;
 If datah > 32 then datah := 32;}
 result := datah;
end;

Procedure TDjinnTileMapper.SetCurbank(n: integer);
begin
 If ROMopened then
 begin
  Curbank := n;
  drawtilemap;
 end;
end;

Function  TDjinnTileMapper.GetCurbank: integer;
begin
 Result := Curbank;
end;

Procedure TDjinnTileMapper.Setdbank(n: integer);
begin
 If ROMopened then
 begin
  CdPos := n;
  DataMapDraw;
 end;
end;

Function  TDjinnTileMapper.Getdbank: integer;
begin
 Result := Cdpos;
end;


Procedure TDjinnTileMapper.DrawTileMap;

Var Tile, p: PROMData; xn, yn, x, xx, y, yy, xu, uxy, yu, i, j: Integer;
const bit: array[0..7] of Byte = (128, 64, 32, 16, 8, 4, 2, 1);
label exxxit;
begin
 Case Codec of
  cdc1BPP: If not b16x16 then
  begin
   xn := 0; yn := 0; xu := 0; yu := 0;
   For y := 0 to 99 do
    For x := 0 to 15 do
    begin
{     Tile := @ROMdata^[datapos + (y * (tilew * tileh * 16) div 8
      + x * (tilew * tileh) div 8)];
     i := 0;
     For yy := 0 to tileh - 1 do
     begin
      For xx := 0 to tilew - 1 do
      begin
       bmap.Canvas.Pixels[xx + x * tilew, yy + y * tileh] :=
       colors[
        ((Tile^[yy * byte(xx - i >= 0) + ((yy * tilew) div 8) * byte(xx - i < 0)
        + ((xx - i) div 8) * byte(xx - i > 7)] and
         bit[(byte(xx - i) mod 8)]) shr
        (7 - ((byte(xx - i) mod 8)))) + 1];
      end;
      inc(i, 8 - tilew);
     end;}
     Tile := @ROMdata^[datapos +
     ((y * (8 * 8 * 16) div 8
      + x * (8 * 8) div 8))
{     ((y * (tilew * tileh * 16) div 8
      + x * (tilew * tileh) div 8))}];
     yy := 0; i := 0; xx := 0;
     repeat
      bmap.Canvas.Pixels[xu + xn * tilew, yu + yn * tileh] :=
      colors[
       ((Tile^[yy] and bit[xx]) shr (7 - xx)) + 1];
      inc(xx); if xx = 8 then
      begin
       xx := 0;
       inc(yy);
      end;
      inc(xu);
      if xu = tilew then
      begin
       xu := 0;
       inc(yu);
       if yu = tileh then
       begin
        yu := 0;
     inc(xn);
     if xn = 16 then
     begin
      xn := 0;
      inc(yn);
      if yn = 16 then goto exxxit;
     end;
       end;
      end;
      inc(i);
     until i = 8 * 8;
    end;
  end Else
  For y := 0 to 15 do
   For x := 0 to 15 do
   begin
    Tile := @ROMdata^[datapos + (y shl 9 + x shl 5)];
    For yy := 0 to 15 do
     For xx := 0 to 15 do
     begin
      bmap.Canvas.Pixels[xx + x shl 4, yy + y shl 4] :=
      colors[
       ((Tile^[yy * 2 + xx div 8] and bit[xx mod 8]) shr (7 - xx mod 8)) + 1];
     end;
   end;
  cdc2BPPNES: If not b16x16 then
  begin
   xn := 0; yn := 0; xu := 0; yu := 0;// i := 0;
   For y := 0 to 99 do
    For x := 0 to 15 do
   begin
     Tile := @ROMdata^[datapos +
     ((y * (8 * 8 * 16) div 8
      + x * (8 * 8) div 8)) * 2];
    yy := 0; i := 0; xx := 0;
{    showmessage(inttostr(     ((y * (8 * 8 * 16) div 8
      + x * (8 * 8) div 8)) * 2));}
    repeat
     bmap.Canvas.Pixels[xu + xn * tilew, yu + yn * tileh] :=
      colors[
       ((Tile^[yy] and bit[xx]) shr (7 - xx)) +
       ((Tile^[yy + 8] and bit[xx]) shr (7 - xx)) shl 1 + 1];
      inc(xx); if xx = 8 then
      begin
       xx := 0;
       inc(yy);
      end;
      inc(xu);
      if xu = tilew then
      begin
       xu := 0;
       inc(yu);
       if yu = tileh then
       begin
        yu := 0;
     inc(xn);
     if xn = 16 then
     begin
      xn := 0;
      inc(yn);
      if yn = 16 then goto exxxit;
     end;
       end;
      end;
      inc(i);
     until i = 8 * 8;
   end
  end Else
  For y := 0 to 15 do
   For x := 0 to 15 do
   begin
    Tile := @ROMdata^[datapos + (y shl 10 + x shl 6)];
    For yy := 0 to 7 do
     For xx := 0 to 7 do
     begin
      bmap.Canvas.Pixels[xx + x shl 4, yy + y shl 4] :=
      colors[
       ((Tile^[yy] and bit[xx]) shr (7 - xx)) +
       ((Tile^[yy + 8] and bit[xx]) shr (7 - xx)) shl 1 + 1];
     end;
    For yy := 0 to 7 do
     For xx := 0 to 7 do
     begin
      bmap.Canvas.Pixels[xx + 8 + x shl 4, yy + y shl 4] :=
      colors[
       ((Tile^[16 + yy] and bit[xx]) shr (7 - xx)) +
       ((Tile^[16 + yy + 8] and bit[xx]) shr (7 - xx)) shl 1 + 1];
     end;
    For yy := 0 to 7 do
     For xx := 0 to 7 do
     begin
      bmap.Canvas.Pixels[xx + x shl 4, yy + 8 + y shl 4] :=
      colors[
       ((Tile^[32 + yy] and bit[xx]) shr (7 - xx)) +
       ((Tile^[32 + yy + 8] and bit[xx]) shr (7 - xx)) shl 1 + 1];
     end;
    For yy := 0 to 7 do
     For xx := 0 to 7 do
     begin
      bmap.Canvas.Pixels[xx + 8 + x shl 4, yy + 8 + y shl 4] :=
      colors[
       ((Tile^[48 + yy] and bit[xx]) shr (7 - xx)) +
       ((Tile^[48 + yy + 8] and bit[xx]) shr (7 - xx)) shl 1 + 1];
     end;
   end;
  cdc2BPPGB: If b16x16 then
  For y := 0 to 15 do
   For x := 0 to 15 do
   begin
    Tile := @ROMdata^[datapos + (y shl 10 + x shl 6)];
    For yy := 0 to 7 do
     For xx := 0 to 7 do
     begin
      bmap.Canvas.Pixels[xx + x shl 4, yy + y shl 4] :=
      colors[
       ((Tile^[yy * 2] and bit[xx]) shr (7 - xx)) +
       ((Tile^[yy * 2 + 1] and bit[xx]) shr (7 - xx)) shl 1 + 1];
     end;
    For yy := 0 to 7 do
     For xx := 0 to 7 do
     begin
      bmap.Canvas.Pixels[xx + 8 + x shl 4, yy + y shl 4] :=
      colors[
       ((Tile^[16 + yy * 2] and bit[xx]) shr (7 - xx)) +
       ((Tile^[16 + yy * 2 + 1] and bit[xx]) shr (7 - xx)) shl 1 + 1];
     end;
    For yy := 0 to 7 do
     For xx := 0 to 7 do
     begin
      bmap.Canvas.Pixels[xx + x shl 4, yy + 8 + y shl 4] :=
      colors[
       ((Tile^[32 + yy * 2] and bit[xx]) shr (7 - xx)) +
       ((Tile^[32 + yy * 2 + 1] and bit[xx]) shr (7 - xx)) shl 1 + 1];
     end;
    For yy := 0 to 7 do
     For xx := 0 to 7 do
     begin
      bmap.Canvas.Pixels[xx + 8 + x shl 4, yy + 8 + y shl 4] :=
      colors[
       ((Tile^[48 + yy * 2] and bit[xx]) shr (7 - xx)) +
       ((Tile^[48 + yy * 2 + 1] and bit[xx]) shr (7 - xx)) shl 1 + 1];
     end;
   end Else
   begin
   xn := 0; yn := 0; xu := 0; yu := 0;
   For y := 0 to 99 do
    For x := 0 to 15 do
   begin
{    Tile := @ROMdata^[datapos + (y shl 7 + x shl 3) shl 1];
    For yy := 0 to tileh - 1 do
     For xx := 0 to tilew - 1 do
     begin
      bmap.Canvas.Pixels[xx + x shl 3, yy + y shl 3] :=
      colors[
       ((Tile^[yy * 2] and bit[xx]) shr (7 - xx)) +
       ((Tile^[yy * 2 + 1] and bit[xx]) shr (7 - xx)) shl 1 + 1];
     end;}
     Tile := @ROMdata^[datapos +
     ((y * (8 * 8 * 16) div 8
      + x * (8 * 8) div 8)) * 2];
    yy := 0; i := 0; xx := 0;
    repeat
     bmap.Canvas.Pixels[xu + xn * tilew, yu + yn * tileh] :=
      colors[
       ((Tile^[yy * 2] and bit[xx]) shr (7 - xx)) +
       ((Tile^[yy * 2 + 1] and bit[xx]) shr (7 - xx)) shl 1 + 1];
     inc(xx); if xx = 8 then
     begin
      xx := 0;
      inc(yy);
     end;
     inc(xu);
     if xu = tilew then
     begin
      xu := 0;
      inc(yu);
       if yu = tileh then
       begin
        yu := 0;
     inc(xn);
     if xn = 16 then
     begin
      xn := 0;
      inc(yn);
      if yn = 16 then goto exxxit;
     end;
       end;
     end;
     inc(i);
    until i = 8 * 8;
   end;
  end;
  cdc4BPPSNES:begin
   xn := 0; yn := 0; xu := 0; yu := 0;
   For y := 0 to 99 do
    For x := 0 to 15 do
   begin
{    Tile := @ROMdata^[datapos + y shl 9 + x shl 5];
    For yy := 0 to tileh - 1 do
     For xx := 0 to tilew - 1 do
     begin
      bmap.Canvas.Pixels[xx + x shl 3, yy + y shl 3] :=
      colors16[
       ((Tile^[yy * 2] and bit[xx]) shr (7 - xx)) +
       ((Tile^[yy * 2 + 1] and bit[xx]) shr (7 - xx)) shl 1 +
       ((Tile^[yy * 2 + 16] and bit[xx]) shr (7 - xx)) shl 2 +
       ((Tile^[yy * 2 + 17] and bit[xx]) shr (7 - xx)) shl 3];
     end;}
     Tile := @ROMdata^[datapos +
     ((y * (8 * 8 * 16) div 8
      + x * (8 * 8) div 8)) * 4];
    yy := 0; i := 0; xx := 0;
    repeat
     bmap.Canvas.Pixels[xu + xn * tilew, yu + yn * tileh] :=
     colors16[
      ((Tile^[yy * 2] and bit[xx]) shr (7 - xx)) +
      ((Tile^[yy * 2 + 1] and bit[xx]) shr (7 - xx)) shl 1 +
      ((Tile^[yy * 2 + 16] and bit[xx]) shr (7 - xx)) shl 2 +
      ((Tile^[yy * 2 + 17] and bit[xx]) shr (7 - xx)) shl 3];
     inc(xx); if xx = 8 then
     begin
      xx := 0;
      inc(yy);
     end;
     inc(xu);
     if xu = tilew then
     begin
      xu := 0;
      inc(yu);
       if yu = tileh then
       begin
        yu := 0;
     inc(xn);
     if xn = 16 then
     begin
      xn := 0;
      inc(yn);
      if yn = 16 then goto exxxit;
     end;
       end;
     end;
     inc(i);
    until i = 8 * 8;
   end;
  end;
  cdc4BPPSNESm:begin
   xn := 0; yn := 0; xu := 0; yu := 0;
   For y := 0 to 99 do
    For x := 0 to 15 do
   begin
{    Tile := @ROMdata^[datapos + y shl 9 + x shl 5];
    For yy := 0 to tileh - 1 do
     For xx := 0 to tilew - 1 do
     begin
      bmap.Canvas.Pixels[(7 - xx) + x shl 3, yy + y shl 3] :=
      colors16[
       ((Tile^[yy * 2] and bit[xx]) shr (7 - xx)) +
       ((Tile^[yy * 2 + 1] and bit[xx]) shr (7 - xx)) shl 1 +
       ((Tile^[yy * 2 + 16] and bit[xx]) shr (7 - xx)) shl 2 +
       ((Tile^[yy * 2 + 17] and bit[xx]) shr (7 - xx)) shl 3];
     end;}
     Tile := @ROMdata^[datapos +
     ((y * (8 * 8 * 16) div 8
      + x * (8 * 8) div 8)) * 4];
    yy := 0; i := 0; xx := 0;
    repeat
     bmap.Canvas.Pixels[xu + xn * tilew, yu + yn * tileh] :=
      colors16[
       ((Tile^[yy * 2] and bit[(7 - xx)]) shr (7 - (7 - xx))) +
       ((Tile^[yy * 2 + 1] and bit[(7 - xx)]) shr (7 - (7 - xx))) shl 1 +
       ((Tile^[yy * 2 + 16] and bit[(7 - xx)]) shr (7 - (7 - xx))) shl 2 +
       ((Tile^[yy * 2 + 17] and bit[(7 - xx)]) shr (7 - (7 - xx))) shl 3];
     inc(xx); if xx = 8 then
     begin
      xx := 0;
      inc(yy);
     end;
     inc(xu);
     if xu = tilew then
     begin
      xu := 0;
      inc(yu);
       if yu = tileh then
       begin
        yu := 0;
     inc(xn);
     if xn = 16 then
     begin
      xn := 0;
      inc(yn);
      if yn = 16 then goto exxxit;
     end;
       end;
     end;
     inc(i);
    until i = 8 * 8;
   end;
  end;
  cdc4BPPGBA:begin
   xn := 0; yn := 0; xu := 0; yu := 0;
   For y := 0 to 99 do
    For x := 0 to 15 do
   begin
{    Tile := @ROMdata^[datapos + y shl 9 + x shl 5];
    For yy := 0 to tileh - 1 do
     For xx := 0 to tilew - 1 do
     begin
      xu := (4 * ((7 - xx) mod 2));
      bmap.Canvas.Pixels[xx + x shl 3, yy + y shl 3] :=
       colors16[
        ((Tile^[yy * 4 + xx div 2] and bit[xu + 3]) shr (7 - (xu + 3))) +
        ((Tile^[yy * 4 + xx div 2] and bit[xu + 2]) shr (7 - (xu + 2))) shl 1 +
        ((Tile^[yy * 4 + xx div 2] and bit[xu + 1]) shr (7 - (xu + 1))) shl 2 +
        ((Tile^[yy * 4 + xx div 2] and bit[xu + 0]) shr (7 - (xu + 0))) shl 3]
     end;}
     Tile := @ROMdata^[datapos +
     ((y * (8 * 8 * 16) div 8
      + x * (8 * 8) div 8)) * 4];
    yy := 0; i := 0; xx := 0;
    repeat
     uxy := (4 * ((7 - xx) mod 2));
     bmap.Canvas.Pixels[xu + xn * tilew, yu + yn * tileh] :=
      colors16[
        ((Tile^[yy * 4 + xx div 2] and bit[uxy + 3]) shr (7 - (uxy + 3))) +
        ((Tile^[yy * 4 + xx div 2] and bit[uxy + 2]) shr (7 - (uxy + 2))) shl 1 +
        ((Tile^[yy * 4 + xx div 2] and bit[uxy + 1]) shr (7 - (uxy + 1))) shl 2 +
        ((Tile^[yy * 4 + xx div 2] and bit[uxy + 0]) shr (7 - (uxy + 0))) shl 3];
     inc(xx); if xx = 8 then
     begin
      xx := 0;
      inc(yy);
     end;
     inc(xu);
     if xu = tilew then
     begin
      xu := 0;
      inc(yu);
       if yu = tileh then
       begin
        yu := 0;
     inc(xn);
     if xn = 16 then
     begin
      xn := 0;
      inc(yn);
      if yn = 16 then goto exxxit;
     end;
       end;
     end;
     inc(i);
    until i = 8 * 8;
   end;
  end;
  cdc4BPPMSX:begin
   xn := 0; yn := 0; xu := 0; yu := 0;
   For y := 0 to 99 do
    For x := 0 to 15 do
   begin
{    Tile := @ROMdata^[datapos + y shl 9 + x shl 5];
    For yy := 0 to tileh - 1 do
     For xx := 0 to tilew - 1 do
     begin
      xu := 4 * (xx mod 2);
      bmap.Canvas.Pixels[xx + x shl 3, yy + y shl 3] :=
       colors16[
        ((Tile^[yy * 4 + xx div 2] and bit[xu + 3]) shr (7 - (xu + 3))) +
        ((Tile^[yy * 4 + xx div 2] and bit[xu + 2]) shr (7 - (xu + 2))) shl 1 +
        ((Tile^[yy * 4 + xx div 2] and bit[xu + 1]) shr (7 - (xu + 1))) shl 2 +
        ((Tile^[yy * 4 + xx div 2] and bit[xu + 0]) shr (7 - (xu + 0))) shl 3]
     end;}
     Tile := @ROMdata^[datapos +
     ((y * (8 * 8 * 16) div 8
      + x * (8 * 8) div 8)) * 4];
    yy := 0; i := 0; xx := 0;
    repeat
     uxy := 4 * (xx mod 2);
     bmap.Canvas.Pixels[xu + xn * tilew, yu + yn * tileh] :=
       colors16[
        ((Tile^[yy * 4 + xx div 2] and bit[uxy + 3]) shr (7 - (uxy + 3))) +
        ((Tile^[yy * 4 + xx div 2] and bit[uxy + 2]) shr (7 - (uxy + 2))) shl 1 +
        ((Tile^[yy * 4 + xx div 2] and bit[uxy + 1]) shr (7 - (uxy + 1))) shl 2 +
        ((Tile^[yy * 4 + xx div 2] and bit[uxy + 0]) shr (7 - (uxy + 0))) shl 3];
     inc(xx); if xx = 8 then
     begin
      xx := 0;
      inc(yy);
     end;
     inc(xu);
     if xu = tilew then
     begin
      xu := 0;
      inc(yu);
       if yu = tileh then
       begin
        yu := 0;
     inc(xn);
     if xn = 16 then
     begin
      xn := 0;
      inc(yn);
      if yn = 16 then goto exxxit;
     end;
       end;
     end;
     inc(i);
    until i = 8 * 8;
   end;
  end;
  cdc4BPPSMS:begin
   xn := 0; yn := 0; xu := 0; yu := 0;
   For y := 0 to 99 do
    For x := 0 to 15 do
   begin
{    Tile := @ROMdata^[datapos + y shl 9 + x shl 5];
    For yy := 0 to tileh - 1 do
     For xx := 0 to tilew - 1 do
     begin
      bmap.Canvas.Pixels[xx + x shl 3, yy + y shl 3] :=
      colors16[
       ((Tile^[yy * 4] and bit[xx]) shr (7 - xx)) +
       ((Tile^[yy * 4 + 1] and bit[xx]) shr (7 - xx)) shl 1 +
       ((Tile^[yy * 4 + 2] and bit[xx]) shr (7 - xx)) shl 2 +
       ((Tile^[yy * 4 + 3] and bit[xx]) shr (7 - xx)) shl 3];
     end;}
     Tile := @ROMdata^[datapos +
     ((y * (8 * 8 * 16) div 8
      + x * (8 * 8) div 8)) * 4];
    yy := 0; i := 0; xx := 0;
    repeat
     bmap.Canvas.Pixels[xu + xn * tilew, yu + yn * tileh] :=
      colors16[
       ((Tile^[yy * 4] and bit[xx]) shr (7 - xx)) +
       ((Tile^[yy * 4 + 1] and bit[xx]) shr (7 - xx)) shl 1 +
       ((Tile^[yy * 4 + 2] and bit[xx]) shr (7 - xx)) shl 2 +
       ((Tile^[yy * 4 + 3] and bit[xx]) shr (7 - xx)) shl 3];
     inc(xx); if xx = 8 then
     begin
      xx := 0;
      inc(yy);
     end;
     inc(xu);
     if xu = tilew then
     begin
      xu := 0;
      inc(yu);
       if yu = tileh then
       begin
        yu := 0;
     inc(xn);
     if xn = 16 then
     begin
      xn := 0;
      inc(yn);
      if yn = 16 then goto exxxit;
     end;
       end;
     end;
     inc(i);
    until i = 8 * 8;
   end;
  end;
 end;
 exxxit:
 if (tlwidthv = 1) and (tlheightv = 1) then
 begin
  If not b16x16 then
   tilemapform.TileMap.Canvas.StretchDraw(Bounds(0, 0, tilew * 32, tileh * 32), bmap) Else
   tilemapform.tilemap.Canvas.Draw(0, 0, bmap);
  tilemapform.Tilemap.canvas.Rectangle(Bounds((Bank mod 16) * tilew * 2, (Bank div 16) * tileh * 2, tilew * 2, tileh * 2));
 end Else
 begin
  x := 0;
  for yy := 0 to 16 div tlheightv - 1 do
   for xx := 0 to 16 div tlwidthv - 1 do
   begin
    for yu := 0 to tlheightv - 1 do
     for xu := 0 to tlwidthv - 1 do
     begin
tilemapform.TileMap.Canvas.CopyRect(Bounds(
    (xx *  tlwidthv + xu) * 16,
    (yy * tlheightv + yu) * 16,
    16, 16), bmap.Canvas,
    Bounds((x mod 16) shl (3 + Byte(b16x16)),
    (x div 16) shl (3 + Byte(b16x16)), 8 + 8 * Byte(b16x16), 8 + 8 * Byte(b16x16)));
      if x = bank then
     tilemapform.Tilemap.canvas.Rectangle(bounds((xx *  tlwidthv + xu) * 16,
     (yy * tlheightv + yu) * 16, 16, 16));
      inc(x);
     end;
   end;
 end;
 DataMapDraw;
 DrawTile;
end;

Procedure TDjinnTileMapper.MapScrollEnable;
begin
 if not b16x16 then
 begin
  tilemapform.tMapscroll.Max := ROMsize - $800 * bsz[Codec];
  tilemapform.tMapscroll.LargeChange := 8 * (tlwidthv * tlheightv) * $100 * bsz[Codec];
  tilemapform.tMapscroll.SmallChange := 8 * (tlwidthv * tlheightv) * $10  * bsz[Codec];
 end Else
 begin
  tilemapform.tMapscroll.Max := ROMsize - 4 * $800 * bsz[Codec];
  tilemapform.tMapscroll.LargeChange := 4 * 8 * (tlwidthv * tlheightv) * $100 * bsz[Codec];
  tilemapform.tMapscroll.SmallChange := 4 * 8 * (tlwidthv * tlheightv) * $10  * bsz[Codec];
 end;
 tilemapform.tMapscroll.Enabled := True;
 tilemapform.LeftBtn.Enabled := True;
 tilemapform.RightBtn.Enabled := True;
 tilemapform.PlusBtn.Enabled := True;
 tilemapform.MinusBtn.Enabled := True;
end;

Procedure TDjinnTileMapper.DataScrollEnable;
begin
 dataform.Datascroll.Max := ROMsize - DWidth * DHeight;
 dataform.Datascroll.LargeChange := DWidth * DHeight;
 dataform.Datascroll.SmallChange := DWidth;
 dataform.Datascroll.Enabled := True;
 dataform.dLeftBtn.Enabled := True;
 dataform.dRightBtn.Enabled := True;
 dataform.HexEd.Enabled := true;
 dataform.HexEd.Color := clWindow;
end;

Procedure TDjinnTileMapper.SetPos(p: LongInt);
begin
 DataPos := p;
 Caption := Application.Title + ' - [' + fname + ']' + SS[Saved];
 tilemapform.caption := '����� ������ - [' + inttohex(DataPos, 8) +
            ' / ' + inttohex(ROMsize, 8) + ']';
 drawtilemap;
end;

Function  TDjinnTileMapper.GetPos: LongInt;
begin
 Result := Datapos;
end;

Procedure TDjinnTileMapper.SetrPos(p: LongInt);
begin
 romDataPos := p;
 dataform.Caption := '�����, ��� � ������ ����� - [' + IntToHex(p, 8)
 + ' / ' + inttohex(romsize, 8) + ']';
 dataform.label1.Caption := '�������: ' + IntToHex(p + CDpos, 8);

 DataScrollEnable;
 DataMapDraw;
end;

Function  TDjinnTileMapper.GetrPos: LongInt;
begin
 Result := romDatapos;
end;


Procedure TDjinnTileMapper.Fill(Var Im: TImage; Clr: TColor);
var c: tcolor; s: tbrushstyle;
begin
 With Im do
 begin
  c := Canvas.Brush.Color;
  s := Canvas.Brush.Style;
  canvas.Brush.Style := bssolid;
  Canvas.Brush.Color := Clr;
  Canvas.FillRect(Bounds(0, 0, Width, Height));
  Canvas.Brush.Color := c;
  canvas.brush.Style := s;
 end;
end;

Procedure TDjinnTileMapper.SetCol1(Color: TColor);
begin
 Colors[1] := Color;
 Fill(teditform.Color1im, Color);
 If fgi = @teditform.Color1im then foreground := 1;
 If bgi = @teditform.Color1im then background := 1;
end;

Procedure TDjinnTileMapper.Setcol(i: integer; n: TColor);
begin
 col16[i] := n;
 teditform.c16im.canvas.brush.color := n;
 teditform.c16im.canvas.FillRect(Bounds(0, i * 16, 30, 16));
 teditform.c16im.canvas.Brush.Color := $FFFFFF; 
 if (i = fcol) and (i = BCol) then teditform.c16im.canvas.TextOut(8, FCol * 16,
  'BF')
 Else if i = fcol then teditform.c16im.canvas.TextOut(10, FCol * 16, 'F')
 else if i = bcol then teditform.c16im.canvas.TextOut(10, BCol * 16, 'B');
end;

Function  TDjinnTileMapper.Getcol(i: integer): TColor;
begin
 Getcol := col16[i];
end;

Function  TDjinnTileMapper.GetCol1: TColor;
begin
 Result := Colors[1];
end;

Procedure TDjinnTileMapper.SetCol2(Color: TColor);
begin
 Colors[2] := Color;
 Fill(teditform.Color2im, Color);
 If fgi = @teditform.Color2im then foreground := 2;
 If bgi = @teditform.Color2im then background := 2;
end;

Function  TDjinnTileMapper.GetCol2: TColor;
begin
 Result := Colors[2];
end;

Procedure TDjinnTileMapper.SetCol3(Color: TColor);
begin
 Colors[3] := Color;
 Fill(teditform.Color3im, Color);
 If fgi = @teditform.Color3im then foreground := 3;
 If bgi = @teditform.Color3im then background := 3;
end;

Function  TDjinnTileMapper.GetCol3: TColor;
begin
 Result := Colors[3];
end;

Procedure TDjinnTileMapper.SetCol4(Color: TColor);
begin
 Colors[4] := Color;
 Fill(teditform.Color4im, Color);
 If fgi = @teditform.Color4im then foreground := 4;
 If bgi = @teditform.Color4im then background := 4;
end;

Function  TDjinnTileMapper.GetCol4: TColor;
begin
 Result := Colors[4];
end;

Procedure TDjinnTileMapper.SetF(n : byte);
Var T: TColor; b: Boolean; i: integer;
begin
 if codec < cdc4BPPSNES then
 begin
 if (n >= 1) and (n <= 4) then
 begin
  if (fgi <> nil) and (fgi <> bgi) then Fill(fgi^, Colors[FCol]);
  b := (fgi = bgi) and (FCol <> n);
  FCol := n;
  Case n of
   1: fgi := @teditform.Color1im;
   2: fgi := @teditform.Color2im;
   3: fgi := @teditform.Color3im;
   4: fgi := @teditform.Color4im;
  end;
  if fgi^.Canvas.Pixels[0, 0] = $808080 then t := $FFFFFF
  else begin
   T := not fgi^.Canvas.Pixels[0, 0];
   if t shr 24 > 0 then t := t - $FF000000;
  end;
  fgi^.Canvas.Font.Size := 10;
  fgi^.Canvas.Font.Color := t;
  fgi^.Canvas.TextOut(5,1, 'FG');
  if b then background := bcol;
 end;
 end Else
 begin
  FCol := n;
  For I := 0 to 15 do
  begin
   teditform.c16im.canvas.Brush.Color := col16[i];
   teditform.c16im.canvas.FillRect(Bounds(0, i * 16, 30, 16));
  end;
  teditform.c16im.canvas.Brush.Color := $FFFFFF;
  If Fcol = BCol then teditform.c16im.canvas.TextOut(8, FCol * 16, 'BF')
  Else
  begin
   teditform.c16im.canvas.TextOut(10, BCol * 16, 'B');
   teditform.c16im.canvas.TextOut(10, FCol * 16, 'F');
  end;
 end;
end;

Function  TDjinnTileMapper.GetF: byte;
begin
 Result := FCol;
end;

Procedure TDjinnTileMapper.SetB(n : byte);
Var T: TColor; b: Boolean; i: integer;
begin
 if codec < cdc4BPPSNES then
 begin
 if (n >= 1) and (n <= 4) then
 begin
  if (bgi <> nil) and (fgi <> bgi) then Fill(bgi^, Colors[BCol]);
  b := (fgi = bgi) and (BCol <> n);
  BCol := n;
  Case n of
   1: bgi := @teditform.Color1im;
   2: bgi := @teditform.Color2im;
   3: bgi := @teditform.Color3im;
   4: bgi := @teditform.Color4im;
  end;
{  if (bgi <> nil) then Fill(bgi^, Colors[BCol]);}
  if bgi^.Canvas.Pixels[0, 0] = $808080 then t := $FFFFFF
  else begin
   T := not bgi^.Canvas.Pixels[0, 0];
   if t shr 24 > 0 then t := t - $FF000000;
  end;
  bgi^.Canvas.Font.Color := T;
  bgi^.Canvas.Font.Size := 10 ;
  bgi^.Canvas.TextOut(5, 14, 'BG');
{  If bgi = fgi then bgi^.Canvas.TextOut(5, 1, 'FG');}
  if b then Foreground := fcol;
 end;
 end Else
 begin
  BCol := n;
  For I := 0 to 15 do
  begin
   teditform.c16im.canvas.Brush.Color := col16[i];
   teditform.c16im.canvas.FillRect(Bounds(0, i * 16, 30, 16));
  end;
  teditform.c16im.canvas.Brush.Color := $FFFFFF;
  If Fcol = BCol then teditform.c16im.canvas.TextOut(8, FCol * 16, 'BF')
  Else
  begin
   teditform.c16im.canvas.TextOut(10, BCol * 16, 'B');
   teditform.c16im.canvas.TextOut(10, FCol * 16, 'F');
  end;
 end;
end;

Function  TDjinnTileMapper.GetB: byte;
begin
 Result := BCol;
end;

Procedure TDjinnTileMapper.SetCodec(c: TCodec);
begin
 Cdc := c;
 Case c of
  cdc1BPP:
  begin
   tilemapform.c16x16.Enabled := true;
   If tilemapform.c16x16.Checked then t16x16 := true;
   teditform.c16im.Visible := false;
   teditform.Color1im.Visible := True;
   teditform.Color2im.Visible := True;
   teditform.Color3im.Visible := False;
   teditform.Color4im.Visible := False;
   foreground := 2;
   background := 1;
   color1 := colors[1];
   color2 := colors[2];
   tilemapform.Leftbtn.Hint := '������� -' + IntToStr(8 + 24 * Byte(b16x16));
   tilemapform.Rightbtn.Hint := '������� +' + IntToStr(8 + 24 * Byte(b16x16));
  end;
  cdc2BPPNES, cdc2bPPgb:
  begin
   tilemapform.c16x16.Enabled := true;
   If tilemapform.c16x16.Checked then t16x16 := true;
   teditform.c16im.Visible := false;
   teditform.Color1im.Visible := True;
   teditform.Color2im.Visible := True;
   teditform.Color3im.Visible := True;
   teditform.Color4im.Visible := True;
   foreground := 4;
   background := 1;
   color1 := colors[1];
   color2 := colors[2];
   color3 := colors[3];
   color4 := colors[4];
   tilemapform.Leftbtn.Hint := '������� -' + IntToStr(16 + 48 * Byte(b16x16));
   tilemapform.Rightbtn.Hint := '������� +' + IntToStr(16 + 48 * Byte(b16x16));
  end;
  Else
  begin
   tilemapform.c16x16.Enabled := false;
   t16x16 := False;
   teditform.Color1im.Visible := False;
   teditform.Color2im.Visible := False;
   teditform.Color3im.Visible := False;
   teditform.Color4im.Visible := False;
   teditform.c16im.Visible := true;
   Foreground := 15;
   Background := 0;
   tilemapform.Leftbtn.Hint := '������� -32';
   tilemapform.Rightbtn.Hint := '������� +32';
  end;
 end;
 If ROMOpened then
 begin
  MapScrollEnable;
  DrawTileMap;
 end;
end;

Function  TDjinnTileMapper.GetCodec: TCodec;
begin
 Result := Cdc;
end;

procedure TDjinnTileMapper.FormShow(Sender: TObject);
var i, j: Integer; e: String;// uuu : file;
begin
 tilew := 8;
 tileh := 8;
 
 crtransform.caption := Application.Title;
 perenos := $ff;
 endt := 0;
 tlwidthv := 1;
 tlheightv := 1;
 left := 0;
 top := 0;
 width := 545;
 //height := 116;
 initposes;
 dataform.show;
 tilemapform.show;
 teditform.show;
 shiftprsd := False;
 Drawing := False;
 ROMopened := false;
 TBLopened := false;
 Codec := cdc2BPPNES;
 Caption := Application.Title;
 fgi := NIL; bgi := NIL;
 bmap := tbitmap.create;
 btile := tbitmap.create;
 b16x16 := false;
 bmap.Width := 128;
 bmap.Height := 128;
 bmap.PixelFormat := pf24bit;
 btile.PixelFormat := pf24bit;
{ bmap.SaveToFile('test.bmp');
 Assignfile(uuu, 'test.bmp');
 Erase(uuu);}
 btile.Width := 8;
 btile.Height := 8;
 Fill(tilemapform.TileMap, 0);
 tilemapform.TileMap.Width := 256;
 tilemapform.TileMap.Height := 256; 
 Fill(teditform.Tile, 0);
 Fill(dataform.DataMap, 0);
 Color1 := 0;
 Color2 := $30A0;
 Color3 := $6049D8;
 Color4 := $B8C8FF;
 for i := 0 to 3 do
  col16[i] := ($100000 + $1000 + $10) * (i * 5);
 for i := 0 to 3 do
  col16[i + 4] := ($000000 + $1000 + $10) * (i * 5);
 for i := 0 to 3 do
  col16[i + 8] := ($100000 + $0000 + $10) * (i * 5);
 for i := 0 to 3 do
  col16[i + 12] := ($100000 + $1000 + $00) * (i * 5);
 Foreground := 4;
 Background := 1;
 tilemapform.Tilemap.canvas.Pen.Color := $FFFFFF;
 tilemapform.Tilemap.canvas.brush.Style := bsClear;
 dataform.datamap.canvas.Pen.Color := $FFFFFF;
 dataform.datamap.canvas.brush.Style := bsClear;
 dWidth := 32;
 dHeight := 30;
 If ParamCount >= 1 then
 begin
  fname := ParamStr(1);
  j := 0;
  For i := Length(fname) downto 1 do if fname[i] = '.' then
  begin
   j := i;
   break;
  end;
  If j > 0 then
  begin
   e := uppercase(Copy(fname, j, Length(fname) - j + 1));
   If e = '.NES' then tilemapform.codecBox.ItemIndex := Byte(cdc2bPPNES) else
   If (e = '.GB') or (e = '.GBC') then
    tilemapform.codecBox.ItemIndex := Byte(cdc2bPPGB) else
   If e = '.GBA' then tilemapform.codecBox.ItemIndex := Byte(cdc4BPPGBA) Else
   If (e = '.SMC') or (e = '.FIG') then
    tilemapform.codecBox.ItemIndex := Byte(cdc4BPPSNES) Else
   If e = '.BIN' then tilemapform.codecBox.ItemIndex := Byte(cdc4BPPMSX) Else
   If e = '.SMS' then tilemapform.codecBox.ItemIndex := Byte(cdc4BPPSMS) Else
   tilemapform.codecBox.ItemIndex := Byte(cdc1bPP);
  end Else tilemapform.codecBox.ItemIndex := Byte(cdc1bPP);
  codec := tcodec(tilemapform.codecBox.ItemIndex);
  ROMopen;
 end;
end;

procedure TDjinnTileMapper.ExitItemClick(Sender: TObject);
begin
 Close;
end;

Var RBts: LongInt;

Procedure TDjinnTileMapper.ROMopen;
begin
 AssignFile(ROM, fname);
 Reset(ROM, 1);
 If ROMopened then FreeMem(ROMdata, ROMsize);
 ROMsize := FileSize(ROM);
 If ROMsize < $4000 then ROMsize := $4000;
 GetMem(ROMData, ROMsize);
 BlockRead(ROM, ROMData^, ROMSize, RBts);
 CloseFile(ROM);
 Saved := True;
 ROMopened := true;
 tilemapform.tMapscroll.position := 0;
 ROMpos := 0;
 MapScrollEnable;
 tilemapform.TMapScroll.position := 0;
 DataScrollEnable;
 dataform.DataScroll.position := 0;
 romdatApos := 0;
 if tilemapform.visible then
  tilemapform.tMapscroll.SetFocus;
 TileMapMenu.Enabled := true;
 DataMapMenu.Enabled := true;
 ReloadItem.Enabled := True;
 SaveItem.Enabled := True;
 SaveAsItem.Enabled := True;
 teditform.mVlEFTbTN.Enabled := true;
 teditform.mVRightbTN.Enabled := true;
 teditform.mVUpbTN.Enabled := true;
 teditform.mVDownbTN.Enabled := true;
 teditform.FlipbTN.Enabled := true;
 teditform.ClearBtn.Enabled := true;
 teditform.Flip2bTN.Enabled := true;
 teditform.RotLeftbTN.Enabled := true;
 teditform.RotRightbTN.Enabled := true;
 With tilemapform do
 begin
  c16x16.Enabled := True;
  twidth.Enabled := True;
  theight.Enabled := True;
  tlwidth.Enabled := True;
  tlheight.Enabled := True;
 end;
 if TBLopened then InitDataEdit;
end;

procedure TDjinnTileMapper.OpenItemClick(Sender: TObject);
var e: String; i, j: Integer;
begin
 If OpenRom.Execute then
 begin
  vstrfont.Checked := false;
  fname := Openrom.FileName;
  j := 0;
  For i := Length(fname) downto 1 do if fname[i] = '.' then
  begin
   j := i;
   break;
  end;
  If j > 0 then
  begin
   e := uppercase(Copy(fname, j, Length(fname) - j + 1));
   If e = '.NES' then tilemapform.codecBox.ItemIndex := Byte(cdc2bPPNES) else
   If (e = '.GB') or (e = '.GBC') then
    tilemapform.codecBox.ItemIndex := Byte(cdc2bPPGB) else
   If e = '.GBA' then tilemapform.codecBox.ItemIndex := Byte(cdc4BPPGBA) Else
   If (e = '.SMC') or (e = '.FIG') then
    tilemapform.codecBox.ItemIndex := Byte(cdc4BPPSNES) Else
   If e = '.BIN' then tilemapform.codecBox.ItemIndex := Byte(cdc4BPPMSX) Else
   If (e = '.SMS') or (e = '.GG')then tilemapform.codecBox.ItemIndex := Byte(cdc4BPPSMS) Else
   tilemapform.codecBox.ItemIndex := Byte(cdc1bPP);
  end Else tilemapform.codecBox.ItemIndex := Byte(cdc1bPP);
  codec := tcodec(tilemapform.codecBox.ItemIndex);
  ROMOpen;
 end;
end;

procedure TDjinnTileMapper.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 If ROMopened then FreeMem(ROMdata, ROMsize);
 bmap.free;
 btile.free;
end;

Function HexVal(s: String; Var err: Boolean): LongInt;
Function sign(h: Char): Byte;
begin
 if h in ['0'..'9'] then Result := Byte(h) - 48 Else
                         Result := Byte(h) - 55;
end;
Const hs: Array[1..8] of LongInt = ($1,
                                    $10,
                                    $100,
                                    $1000,
                                    $10000,
                                    $100000,
                                    $1000000,
                                    $10000000);
Var i: Byte; Label Error;
begin
 If Length(s) <= 8 then
 begin
  For i := 1 to Length(s) do If not (s[i] in ['0'..'9', 'A'..'F', 'a'..'f']) then Goto Error;
  Result := 0;
  for i := 1 to Length(s) do Inc(Result, sign(s[i]) * hs[length(s) + 1 - i]);
 end Else
 begin Error:
  err := True;
  Exit;
 end;
end;

procedure TDjinnTileMapper.GoTo1Click(Sender: TObject);
Var s: String; v: LongInt; err: Boolean;
begin
 s := UpperCase(Inputbx(Application.Title, '������� �� ������� � ����� ������:', hchs, 10));
 if s = '' then Exit;
 If (s[1] = 'H') then
 begin
  If length(s) >= 2 then
  begin
   Delete(s, 1, 1);
   v := HexVal(s, err);
  end Else err := true;
  If not err then tilemapform.tMapscroll.Position := v Else ShowMessage(errs);
 end Else
 begin
  Try v := StrToint(s) Except on eConvertError do
  begin
   ShowMessage(errs);
   exit;
  end end;
  tilemapform.tMapscroll.Position := v;
 end;
end;

procedure TDjinnTileMapper.GoTo2Click(Sender: TObject);
Var s: String; v: LongInt; err: Boolean;
begin
 s := UpperCase(Inputbx(Application.Title, '������� �� �������:', hchs, 10));
 if s = '' then Exit;
 If (s[1] = 'H') then
 begin
  If length(s) >= 2 then
  begin
   Delete(s, 1, 1);
   v := HexVal(s, err);
  end Else err := true;
  If not err then dataform.Datascroll.Position := v Else ShowMessage(errs);
 end Else
 begin
  Try v := StrToint(s) Except on eConvertError do
  begin
   ShowMessage(errs);
   exit;
  end end;
  dataform.Datascroll.Position := v;
 end;
end;

procedure TDjinnTileMapper.ReloadItemClick(Sender: TObject);
begin
 AssignFile(ROM, fname);
 Reset(ROM, 1);
 If not FileExists(fname) then Exit;
 If ROMopened then FreeMem(ROMdata, ROMsize);
 ROMsize := FileSize(ROM);
 If ROMsize < 8192 then ROMsize := 8192;
 GetMem(ROMData, ROMsize);
 BlockRead(ROM, ROMData^, ROMSize, RBts);
 CloseFile(ROM);
 DrawTileMap;
 Saved := true;
 Caption := Application.Title + ' - [' + fname + ']' +
            SS[Saved];
end;

procedure TDjinnTileMapper.SaveItemClick(Sender: TObject);
begin
 AssignFile(ROM, fname);
 Rewrite(ROM, 1);
 Blockwrite(ROM, ROMData^, ROMsize, Rbts);
 CloseFile(ROM);
 Saved := True;
 Caption := Application.Title + ' - [' + fname + ']' +
            SS[Saved];
end;

procedure TDjinnTileMapper.SaveASitemClick(Sender: TObject);
begin
 If SaveROM.Execute then
 begin
  fname := SaveROM.FileName;
  AssignFile(ROM, fname);
  Rewrite(ROM, 1);
  Blockwrite(ROM, ROMData^, ROMsize, Rbts);
  CloseFile(ROM);
  Saved := True;
 Caption := Application.Title + ' - [' + fname + ']' +
            SS[Saved];
 end;
end;

procedure TDjinnTileMapper.Restore1Click(Sender: TObject);
Var i: Integer;
begin
 If codec < cdc4bppsnes then
 begin
  Color1 := 0;
  Color2 := $30A0;
  Color3 := $6049D8;
  Color4 := $B8C8FF;
 end Else for i := 0 to 3 do
  colors16[i] := ($100000 + $1000 + $10) * (i * 5);
 for i := 0 to 3 do
  colors16[i + 4] := ($000000 + $1000 + $10) * (i * 5);
 for i := 0 to 3 do
  colors16[i + 8] := ($100000 + $0000 + $10) * (i * 5);
 for i := 0 to 3 do
  colors16[i + 12] := ($100000 + $1000 + $00) * (i * 5);
 If ROMopened then DrawTileMap;
end;

Procedure ddd;
Var p: PROMData; a, b, c, d: Byte;
xu, ox, oy: integer;
Procedure getbitcol;
var i: byte; cn: byte;
begin
 cn := 0;
 for i := 0 to 15 do
  if btile.canvas.Pixels[ox, oy] = Col16[i]then
  begin
   cn := i;
   Break;
  end;
 a := (cn shr 3) and 1;
 b := (cn shr 2) and 1;
 c := (cn shr 1) and 1;
 d := (cn shr 0) and 1;
end;
var{ bit: byte; tyy,} tya: integer;
begin
 if b16x16 then
 begin
  ddd16;
  Exit;
 end;
 If (tx >= 0) and (ty >= 0) and
 (tx < tilew) and (ty < tileh) then
 With DjinnTileMapper do
 begin
  ox := tx;
  oy := ty;
  p := Addr(ROMData^[DataPos +
((bank * tilew * tileh + ty * tilew + tx) div 64) * 8 * bsz[codec]
  {Bank * ((tilew * tileh) div (8 div bsz[codec]) - 1 *
   byte((bank * tilew * tileh) mod (8 div bsz[codec]) > 0)) * bsz[Codec]}]);
{  bit := (ty * tilew + tx) mod (8 div bsz[codec]);
  dataedit.Text := inttostr(tx) + ' ' + inttostr(ty) + ' ' + inttostr(bit) + ' ' +
  inttostr((ty * tilew + tx) div 8);}
{  tyy := (bank * tilew * tileh + ty * tilew + tx) div
  64;
  tya := (tyy * tilew * tileh + ty * tilew + tx);        }
  tya := ((bank * tilew * tileh + ty * tilew + tx) mod 64);
  ty := tya div 8;
  tx := tya mod 8;
{ tx := 0;
  ty := 0;         }
 { dataedit.Text := inttostr(tx) + ' ' + inttostr(ty)  + ' ' +
   inttostr(((bank * tilew * tileh * bsz[codec] + ty * tilew * bsz[codec] + tx * bsz[codec])
    div (8 * bsz[codec] * 8)) * 8 * bsz[codec]) + ' ' + inttostr(tya);  }
{  tilew := 8;
  tileh := 8;     }
   Case Codec of
{   cdc1BPP:
   If bTile.Canvas.Pixels[tx, ty] = Colors[1] then
   begin
    p^[(ty * tilew + tx) div 8] :=
    p^[(ty * tilew + tx) div 8] and not (1 shl (7 - bit));
   end Else
   begin
    p^[(ty * tilew + tx) div 8] :=
    p^[(ty * tilew + tx) div 8] or (1 shl (7 - bit));
   end;
   cdc2BPPNES:
   If bTile.Canvas.Pixels[tx, ty] = Colors[1] then
   begin
    p^[(ty * tilew + tx) div 8] :=
    p^[(ty * tilew + tx) div 8] and not (1 shl (7 - bit));
    p^[((ty + 8) * tilew + tx) div 8] :=
    p^[((ty + 8) * tilew + tx) div 8] and not (1 shl (7 - bit))
   end Else If bTile.Canvas.Pixels[tx, ty] = Colors[2] then
   begin
    p^[(ty * tilew + tx) div 8] :=
    p^[(ty * tilew + tx) div 8] or (1 shl (7 - bit));
    p^[((ty + 8) * tilew + tx) div 8] :=
    p^[((ty + 8) * tilew + tx) div 8] and not (1 shl (7 - bit))
   end Else If bTile.Canvas.Pixels[tx, ty] = Colors[4] then
   begin
    p^[(ty * tilew + tx) div 8] :=
    p^[(ty * tilew + tx) div 8] or (1 shl (7 - bit));
    p^[((ty + 8) * tilew + tx) div 8] :=
    p^[((ty + 8) * tilew + tx) div 8] or (1 shl (7 - bit))
   end Else
   begin
    p^[(ty * tilew + tx) div 8] :=
    p^[(ty * tilew + tx) div 8] and not (1 shl (7 - bit));
    p^[((ty + 8) * tilew + tx) div 8] :=
    p^[((ty + 8) * tilew + tx) div 8] or (1 shl (7 - bit))
   END;}
   cdc1BPP:
   If bTile.Canvas.Pixels[ox, oy] = Colors[1] then
   begin
    p^[ty] := p^[ty] and not (1 shl (7 - tx))
   end Else
   begin
    p^[ty] := p^[ty] or (1 shl (7 - tx))
   end;
   cdc2BPPNES:
   If bTile.Canvas.Pixels[ox, oy] = Colors[1] then
   begin
    p^[ty] := p^[ty] and not (1 shl (7 - tx));
    p^[ty + 8] := p^[ty + 8] and not (1 shl (7 - tx))
   end Else If bTile.Canvas.Pixels[ox, oy] = Colors[2] then
   begin
    p^[ty] := p^[ty] or (1 shl (7 - tx));
    p^[ty + 8] := p^[ty + 8] and not (1 shl (7 - tx))
   end Else If bTile.Canvas.Pixels[ox, oy] = Colors[4] then
   begin
    p^[ty] := p^[ty] or (1 shl (7 - tx));
    p^[ty + 8] := p^[ty + 8] or (1 shl (7 - tx))
   end Else
   begin
    p^[ty] := p^[ty] and not (1 shl (7 - tx));
    p^[ty + 8] := p^[ty + 8] or (1 shl (7 - tx))
   END;
   cdc2BPPGB:
   If bTile.Canvas.Pixels[ox, oy] = Colors[1] then
   begin
    p^[ty * 2] := p^[ty * 2] and not (1 shl (7 - tx));
    p^[ty * 2 + 1] := p^[ty * 2 + 1] and not (1 shl (7 - tx))
   end Else If bTile.Canvas.Pixels[ox, oy] = Colors[2] then
   begin
    p^[ty * 2] := p^[ty * 2] or (1 shl (7 - tx));
    p^[ty * 2 + 1] := p^[ty * 2 + 1] and not (1 shl (7 - tx))
   end Else If bTile.Canvas.Pixels[ox, oy] = Colors[4] then
   begin
    p^[ty * 2] := p^[ty * 2] or (1 shl (7 - tx));
    p^[ty * 2 + 1] := p^[ty * 2 + 1] or (1 shl (7 - tx))
   end Else
   begin
    p^[ty * 2] := p^[ty * 2] and not (1 shl (7 - tx));
    p^[ty * 2 + 1] := p^[ty * 2 + 1] or (1 shl (7 - tx))
   END;
   cdc4bppsnes:
   begin
    getbitcol;
    If d = 1 then
     p^[ty * 2] := p^[ty * 2] or (1 shl (7 - tx)) Else
    If d = 0 then
     p^[ty * 2] := p^[ty * 2] and not (1 shl (7 - tx));
    If c = 1 then
     p^[ty * 2 + 1] := p^[ty * 2 + 1] or (1 shl (7 - tx)) Else
    If c = 0 then
     p^[ty * 2 + 1] := p^[ty * 2 + 1] and not (1 shl (7 - tx));
    If b = 1 then
     p^[ty * 2 + 16] := p^[ty * 2 + 16] or (1 shl (7 - tx)) Else
    If b = 0 then
     p^[ty * 2 + 16] := p^[ty * 2 + 16] and not (1 shl (7 - tx));
    If a = 1 then
     p^[ty * 2 + 17] := p^[ty * 2 + 17] or (1 shl (7 - tx)) Else
    If a = 0 then
     p^[ty * 2 + 17] := p^[ty * 2 + 17] and not (1 shl (7 - tx));
   end;
   cdc4bppsnesm:
   begin
    getbitcol;
    If d = 1 then
     p^[ty * 2] := p^[ty * 2] or (1 shl (tx)) Else
    If d = 0 then
     p^[ty * 2] := p^[ty * 2] and not (1 shl (tx));
    If c = 1 then
     p^[ty * 2 + 1] := p^[ty * 2 + 1] or (1 shl (tx)) Else
    If c = 0 then
     p^[ty * 2 + 1] := p^[ty * 2 + 1] and not (1 shl (tx));
    If b = 1 then
     p^[ty * 2 + 16] := p^[ty * 2 + 16] or (1 shl (tx)) Else
    If b = 0 then
     p^[ty * 2 + 16] := p^[ty * 2 + 16] and not (1 shl (tx));
    If a = 1 then
     p^[ty * 2 + 17] := p^[ty * 2 + 17] or (1 shl (tx)) Else
    If a = 0 then
     p^[ty * 2 + 17] := p^[ty * 2 + 17] and not (1 shl (tx));
   end;
   cdc4bppsms:
   begin
    getbitcol;
    If d = 1 then
     p^[ty * 4] := p^[ty * 4] or (1 shl (7 - tx)) Else
    If d = 0 then
     p^[ty * 4] := p^[ty * 4] and not (1 shl (7 - tx));
    If c = 1 then
     p^[ty * 4 + 1] := p^[ty * 4 + 1] or (1 shl (7 - tx)) Else
    If c = 0 then
     p^[ty * 4 + 1] := p^[ty * 4 + 1] and not (1 shl (7 - tx));
    If b = 1 then
     p^[ty * 4 + 2] := p^[ty * 4 + 2] or (1 shl (7 - tx)) Else
    If b = 0 then
     p^[ty * 4 + 2] := p^[ty * 4 + 2] and not (1 shl (7 - tx));
    If a = 1 then
     p^[ty * 4 + 3] := p^[ty * 4 + 3] or (1 shl (7 - tx)) Else
    If a = 0 then
     p^[ty * 4 + 3] := p^[ty * 4 + 3] and not (1 shl (7 - tx));
   end;
   cdc4bppgba:
   begin
    getbitcol;
    xu := (4 * ((7 - tx) mod 2));
    If d = 1 then
     p^[ty * 4 + tx div 2] := p^[ty * 4 + tx div 2] or (1 shl (7 - (xu + 3))) Else
    If d = 0 then
     p^[ty * 4 + tx div 2] := p^[ty * 4 + tx div 2] and not (1 shl (7 - (xu + 3)));
    If c = 1 then
     p^[ty * 4 + tx div 2] := p^[ty * 4 + tx div 2] or (1 shl (7 - (xu + 2))) Else
    If c = 0 then
     p^[ty * 4 + tx div 2] := p^[ty * 4 + tx div 2] and not (1 shl (7 - (xu + 2)));
    If b = 1 then
     p^[ty * 4 + tx div 2] := p^[ty * 4 + tx div 2] or (1 shl (7 - (xu + 1))) Else
    If b = 0 then
     p^[ty * 4 + tx div 2] := p^[ty * 4 + tx div 2] and not (1 shl (7 - (xu + 1)));
    If a = 1 then
     p^[ty * 4 + tx div 2] := p^[ty * 4 + tx div 2] or (1 shl (7 - xu)) Else
    If a = 0 then
     p^[ty * 4 + tx div 2] := p^[ty * 4 + tx div 2] and not (1 shl (7 - xu));
   end;
   cdc4bppmsx:
   begin
    getbitcol;
    xu := (4 * (tx mod 2));
    If d = 1 then
     p^[ty * 4 + tx div 2] := p^[ty * 4 + tx div 2] or (1 shl (7 - (xu + 3))) Else
    If d = 0 then
     p^[ty * 4 + tx div 2] := p^[ty * 4 + tx div 2] and not (1 shl (7 - (xu + 3)));
    If c = 1 then
     p^[ty * 4 + tx div 2] := p^[ty * 4 + tx div 2] or (1 shl (7 - (xu + 2))) Else
    If c = 0 then
     p^[ty * 4 + tx div 2] := p^[ty * 4 + tx div 2] and not (1 shl (7 - (xu + 2)));
    If b = 1 then
     p^[ty * 4 + tx div 2] := p^[ty * 4 + tx div 2] or (1 shl (7 - (xu + 1))) Else
    If b = 0 then
     p^[ty * 4 + tx div 2] := p^[ty * 4 + tx div 2] and not (1 shl (7 - (xu + 1)));
    If a = 1 then
     p^[ty * 4 + tx div 2] := p^[ty * 4 + tx div 2] or (1 shl (7 - xu)) Else
    If a = 0 then
     p^[ty * 4 + tx div 2] := p^[ty * 4 + tx div 2] and not (1 shl (7 - xu));
   end;
  end;
  tx := ox;
  ty := oy;
 end;
end;

procedure TDjinnTileMapper.MkTmplItemClick(Sender: TObject);
Var T: TextFile; i: Byte;
begin
 If MakeTBL.Execute then
 begin
  AssignFile(t, MakeTBL.FileName);
  Rewrite(t);
  Writeln(t, 'H' + IntToHex(DataPos, 6));
  For i := 0 to 255 do Writeln(t, IntToHex(i, 2) + '=');
  writeln(t, '*FF');
  writeln(t, '/00');  
  CloseFile(t);
 end;
end;

procedure TDjinnTileMapper.InitDataEdit;
Var n, l: Byte;
begin
 DataEdit.Enabled := True;
 DataEdit.Color := clWindow;
 DataEdit.Text := '';
 DEditset := [#8, #13];
 For n := 0 to 255 do If Table[n] = '' then Table[n] := '{' + IntToHex(n, 2) + '}';
 table[perenos] := '{^}';
 table[endt] := '{END}';
 maxtlen := 0;
 For n := 0 to 255 do
 begin
  For l := 1 to Length(Table[n]) do
   if not (Table[n][l] in DEditSet) then DEditSet := DEditSet + [Table[n][l]];
  If maxTlen < Length(Table[n]) then maxTlen := Length(Table[n]);
 end;
 PasteBtn.Enabled := True;
 GetBtn.Enabled := True;
 GetString1.Enabled := True;
 PutString1.Enabled := True;
end;

procedure TDjinnTileMapper.OpenTblItemClick(Sender: TObject);
Label Error, EndProc;
Var T: TextFile; s: String; err: Boolean; n: Byte; backup: TTable; p: LongInt;
begin
 If OpenTBL.Execute then
 begin
  TBLname := openTBL.FileName;
  AssignFile(t, TBLname);
  Reset(t);
  Readln(t, s);
  If UpCase(s[1]) = 'H' then
  begin
   p := HexVal(Copy(s, 2, Length(s) - 1), err);
   If Err then p := 0;
  end Else
  begin
   p := StrToInt(s);
   If p < 0 then  p := 0;
  end;
  While not EOF(t) do
  begin
   Readln(t, s);
   if (length(s) = 3) and (s[1] = '*') then perenos := HexVal(copy(s, 2, 2), err) Else
   if (length(s) = 3) and (s[1] = '/') then endt := HexVal(copy(s, 2, 2), err) else
   begin
    n := HexVal(copy(s, 1, 2), err);
    If not Err and (length(s) > 3) and (s[3] = '=') then
     backup[n] := copy(s, 4, Length(s) - 3);
   end;
  end;
  CloseFile(t);
  TBLopened := True;
  Table := backup;
  TBLnameL.Caption := '�������: ' + TBLname;
  RldTBLitem.Enabled := True;
  Searchform.f3.Enabled := True;
  Searchform.ntsed.Enabled := True;
  Searchform.ntsed.Color := clWindow;
  if ROMopened then
  begin
   tilemapform.TMapScroll.Position := p;
   InitDataEdit;
   n20.Enabled := true;
   n24.Enabled := true;
  end;
 end;
 Goto EndProc;
Error:
 CloseFile(t);
EndProc:
end;

Procedure Paste;
Var s: String;
Function cfind(p: Integer): Integer;
Var g: Integer; n: Byte;
begin
 Result := -1;
 For g := maxtlen downto 1 do
  For n := 0 to 255 do If Copy(s, p, g) = Table[n] then
  begin
   Result := n;
   Exit;
  end;
end;
var n, i, l: Integer;
begin
 With DjinnTileMapper do
 begin
  s := DataEdit.Text;
  if LCnt.Checked then
  begin
   l := length(S);
   If l > MaxSlen then l := maxslen;
   for i := 0 to l - 1 do
   begin
    For n := 0 to 255 do If Table[n] = s[i + 1] then
    begin
     ROMData^[romDataPos + CDpos + i] := n;
     break;
    end;
   end;
  end Else
  begin
   i := 1; l := 0;
   while i <= Length(s) do
   begin
    n := cfind(i);
    If n >= 0 then
    begin
     inc(i, Length(Table[n]) - 1);
     ROMData^[romDataPos + CDpos + l] := n;
     Inc(l);
     If l >= MaxSLen then break;
    end;
    Inc(i);
   end;
  end;
  DrawTileMap;
 end;
end;

procedure TDjinnTileMapper.DataEditKeyPress(Sender: TObject;
  var Key: Char);
begin
 If not (Key in DEditSet) then Key := #0;
 If Key = #13 then Paste;
end;

Function TDjinnTileMapper.GetLength: Integer;
Var i: Integer; n: Integer; s: String;
Function cfind(p: Integer): Integer;
Var g: Integer; n: Byte;
begin
 Result := -1;
 For g := maxtlen downto 1 do
  For n := 0 to 255 do If Copy(s, p, g) = Table[n] then
  begin
   Result := n;
   Exit;
  end;
end;

begin
 s := DataEdit.Text;
 Result := 0;
 i := 1;
 while i <= Length(s) do
 begin
  n := cfind(i);
  If n >= 0 then
  begin
   inc(i, Length(Table[n]) - 1);
   Inc(Result);
  end;
  Inc(i);
 end;
end;

procedure TDjinnTileMapper.DataEditChange(Sender: TObject);
Var n: Integer;
begin
 If LCnt.Checked then n := length(DataEdit.Text) Else n := GetLength;
 LenLab.Caption := '�����: ' +
  IntToStr(n) + ' = h0' + IntToHex(n, 3);
end;

procedure TDjinnTileMapper.PasteBtnClick(Sender: TObject);
begin
 Paste;
end;

procedure TDjinnTileMapper.RldtblitemClick(Sender: TObject);
Label Error, EndProc;
Var T: TextFile; s: String; err: Boolean; n: Byte; backup: TTable; p: LongInt;
begin
 If not FileExists(TBLname) then
 begin
  AssignFile(t, TBLname);
  Rewrite(t);
  For n := 0 to 255 do Writeln(t, IntToHex(n, 2) + '=' + Table[n]);
  CloseFile(t);
 end;
 AssignFile(t, TBLname);
 Reset(t);
 Readln(t, s);
 If UpCase(s[1]) = 'H' then
 begin
  p := HexVal(Copy(s, 2, Length(s) - 1), err);
  If Err then p := 0;
 end Else
 begin
  p := StrToInt(s);
  If p < 0 then p := 0;
 end;
  While not EOF(t) do
  begin
   Readln(t, s);
   if (length(s) = 3) and (s[1] = '*') then perenos := HexVal(copy(s, 2, 2), err) Else
   if (length(s) = 3) and (s[1] = '/') then endt := HexVal(copy(s, 2, 2), err) else
   begin
    n := HexVal(copy(s, 1, 2), err);
    If not Err and (length(s) > 3) and (s[3] = '=') then
     backup[n] := copy(s, 4, Length(s) - 3);
   end;
  end;
 CloseFile(t);
 TBLopened := True;
 Table := backup;
 TBLnameL.Caption := '�������: ' + TBLname;
 if ROMopened then
 begin
  tilemapform.TMapScroll.Position := p;
  InitDataEdit;
 end;
 Goto EndProc;
Error:
 CloseFile(t);
EndProc:
end;

procedure TDjinnTileMapper.GetBtnClick(Sender: TObject);
Var i: Integer; s: String;
begin
 s := '';
 For i := 0 to maxSlen - 1 do s := s + Table[ROMdata^[romDataPos + CDpos + i]];
 DataEdit.Text := s;
end;

procedure TDjinnTileMapper.Getstring1Click(Sender: TObject);
Var i: Integer; s: String;
begin
 DataEdit.SetFocus;
 s := '';
 For i := 0 to maxSlen - 1 do s := s + Table[ROMdata^[romDataPos + CDpos + i]];
 DataEdit.Text := s;
end;

procedure TDjinnTileMapper.PutString1Click(Sender: TObject);
begin
 Paste;
end;

procedure TDjinnTileMapper.AboutItemClick(Sender: TObject);
begin
 AboutForm.ShowModal;
end;

Type RGBZ = Record R, G, B, Z: Byte end;

procedure TDjinnTileMapper.N1Click(Sender: TObject);
Type
 TSimpleRGB = Packed Record
  Red: Byte;
  Green: Byte;
  Blue: Byte;
 end;
type
  TColorRGB = packed record
   rgbRed: Byte;
   rgbGreen: Byte;
   rgbBlue: Byte;
   rgbReserved: Byte;
  end;
Var
 t: TextFile; s, c1, c2: String; c: TColor; i: integer;
 F: File; Col: TSimpleRGB;
begin
 If OpenPAL.Execute then
 begin
  If Pos('.ACT', UpperCase(OpenPal.FileName)) > 0 then
  begin
   AssignFile(F, OpenPAL.FileName);
   Reset(F, 1);
   If Codec < cdc4bppsnes then
   begin
    BlockRead(F, Col, 3, I);
    With Col, TColorRGB(C) do
    begin
     rgbRed := Red;
     rgbGreen := Green;
     rgbBlue := Blue;
     rgbReserved := 0;
    end;
    Color1 := C;
    BlockRead(F, Col, 3, I);
    With Col, TColorRGB(C) do
    begin
     rgbRed := Red;
     rgbGreen := Green;
     rgbBlue := Blue;
     rgbReserved := 0;
    end;
    Color2 := C;
    BlockRead(F, Col, 3, I);
    With Col, TColorRGB(C) do
    begin
     rgbRed := Red;
     rgbGreen := Green;
     rgbBlue := Blue;
     rgbReserved := 0;
    end;
    Color3 := C;
    BlockRead(F, Col, 3, I);
    With Col, TColorRGB(C) do
    begin
     rgbRed := Red;
     rgbGreen := Green;
     rgbBlue := Blue;
     rgbReserved := 0;
    end;
    Color4 := C;
   end Else For I := 0 to 15 do
   begin
    BlockRead(F, Col, 3, I);
    With Col, TColorRGB(C) do
    begin
     rgbRed := Red;
     rgbGreen := Green;
     rgbBlue := Blue;
     rgbReserved := 0;
    end;
    Colors16[I] := C;   
   end;
   CloseFile(F);
   Exit;
  end;
  AssignFile(t, OpenPAL.FileName);
  Reset(t);
  RGBZ(c).z := 0;
  if Codec < cdc4bppsnes then
  begin
   Readln(t, s);
   c1 := copy(s, 1, Pos(' ', s) - 1);
   Delete(s, 1, Pos(' ', s));
   c2 := copy(s, 1, Pos(' ', s) - 1);
   Delete(s, 1, Pos(' ', s));
   RGBZ(c).R := StrToInt(c1);
   RGBZ(c).G := StrToInt(c2);
   RGBZ(c).B := StrToInt(s);
   Color1 := c;

   Readln(t, s);
   c1 := copy(s, 1, Pos(' ', s) - 1);
   Delete(s, 1, Pos(' ', s));
   c2 := copy(s, 1, Pos(' ', s) - 1);
   Delete(s, 1, Pos(' ', s));
   RGBZ(c).R := StrToInt(c1);
   RGBZ(c).G := StrToInt(c2);
   RGBZ(c).B := StrToInt(s);
   Color2 := c;

   Readln(t, s);
   c1 := copy(s, 1, Pos(' ', s) - 1);
   Delete(s, 1, Pos(' ', s));
   c2 := copy(s, 1, Pos(' ', s) - 1);
   Delete(s, 1, Pos(' ', s));
   RGBZ(c).R := StrToInt(c1);
   RGBZ(c).G := StrToInt(c2);
   RGBZ(c).B := StrToInt(s);
   Color3 := c;

   Readln(t, s);
   c1 := copy(s, 1, Pos(' ', s) - 1);
   Delete(s, 1, Pos(' ', s));
   c2 := copy(s, 1, Pos(' ', s) - 1);
   Delete(s, 1, Pos(' ', s));
   RGBZ(c).R := StrToInt(c1);
   RGBZ(c).G := StrToInt(c2);
   RGBZ(c).B := StrToInt(s);
   Color4 := c;
  end else for i := 0 to 15 do
  begin
   Readln(t, s);
   c1 := copy(s, 1, Pos(' ', s) - 1);
   Delete(s, 1, Pos(' ', s));
   c2 := copy(s, 1, Pos(' ', s) - 1);
   Delete(s, 1, Pos(' ', s));
   RGBZ(c).R := StrToInt(c1);
   RGBZ(c).G := StrToInt(c2);
   RGBZ(c).B := StrToInt(s);
   Colors16[i] := c;
  end;
  CloseFile(t);
  If ROMopened then DrawTilemap;
 end;
end;

procedure TDjinnTileMapper.N2Click(Sender: TObject);
Var t: TextFile; i: Byte;
begin
 If SavePAL.Execute then
 begin
  AssignFile(t, SavePAL.FileName);
  Rewrite(t);
  if Codec < cdc4BPPSNES then
   For i := 1 to 4 do
    Writeln(t, IntToStr(RGBZ(Colors[i]).R) + ' ' +
               IntToStr(RGBZ(Colors[i]).G) + ' ' +
               IntToStr(RGBZ(Colors[i]).B))
  else
   For i := 0 to 15 do
    Writeln(t, IntToStr(RGBZ(Col16[i]).R) + ' ' +
               IntToStr(RGBZ(Col16[i]).G) + ' ' +
               IntToStr(RGBZ(Col16[i]).B));
  CloseFile(t);
 end;
end;

procedure TDjinnTileMapper.SearchItemClick(Sender: TObject);
Label 1, 2;
Var s: String; Var i, j, l, n: LongInt; p, fdata: PROMdata; err, nf: Boolean;
Function cfind(p: Integer): Integer;
Var g: Integer; n: Byte;
begin
 Result := -1;
 For g := maxtlen downto 1 do
  For n := 0 to 255 do If Copy(s, p, g) = Table[n] then
  begin
   Result := n;
   Exit;
  end;
end;
Var bu: Boolean; sfi: Integer;
Procedure Find;
Var ku, ju: LongInt;
begin
 For ku := romDataPos to ROMsize - l do
 begin
  p := Addr(ROMdata^[ku]);
  nf := False;
  sfi := searchform.intervalspin.Value + 1;
  For ju := 0 to l - 1 do
   If p^[ju * sfi] <> fData^[ju] then
   begin
    nf := True;
    Break;
   end;
  If ku = romDataPos then
  begin
   nf := True;
   bu := True;
  end;
  If not nf then Break;
 end;
 i := ku;
 j := ju;
end;
Procedure fResult;
begin
 If not nf then
 begin
  dataform.DataScroll.SetFocus;
  dataform.DataScroll.Position := i;
  DataBank := i - romDataPos;
  dataform.Label1.Caption := '�������: ' + IntToHex(romDataPos + CDPos, 6);
  If not tTblOpened then Bank := RomData^[romDatapos + CDPos] Else
                         Bank := TileTable[RomData^[romDatapos + CDPos]];
 end Else If not bu then ShowMessage('�� �����!!!');
end;

begin
 bu := False;
 SearchForm.ShowModal;
 If FindQ then
 begin
  With SearchForm do
  If f1.Checked and (Length(HexfEd.Text) > 0) then
  begin
   s := HexfEd.Text;
   If Length(s) mod 2 = 1 then s := '0' + s;
   l := Length(s) shr 1;
   GetMem(fData, l);
   For i := 0 to Length(s) - 1 do
    If i mod 2 = 0 then
     fData^[i shr 1] := HexVal(Copy(s, i + 1, 2), err);
   Find;
   FreeMem(fData, l);
   fResult;
  end Else
  If f2.Checked and (Length(TfEd.Text) > 1) then
  begin
   srelative;
  end Else
  If f3.Checked and (Length(ntsed.Text) > 0) then
  begin
   s := nTsEd.Text;
   i := 1; l := 0;
   GetMem(fData, lENGTH(S));
   If lCnt.Checked then
   begin
    for i := 1 to Length(s) do For l := 0 to 255 do If Table[l] = s[i] then
     fData^[i - 1] := l;
    l := Length(S);
   end Else
   while i <= Length(s) do
   begin
    n := cfind(i);
    If n >= 0 then
    begin
     inc(i, Length(Table[n]) - 1);
     fData^[l] := n;
     Inc(l);
    end;
    Inc(i);
   end;
   Find;
   FreeMem(fData, lENGTH(S));
   fResult;
  end;
 end;
end;

procedure tdjinntilemapper.srelative;
function rgethex(pos: longword; size: integer): string;
var i: integer;
begin
 result := '';
 for i := 0 to size - 1 do
  result := result + inttohex(romdata^[pos + i], 2);
end;

label l1;
var
  msize:byte;
  tp:longword;
 i:longword;
s1:string;
f:boolean;
k1,k2:integer;
begin
{ SearchForm.ShowModal;}
 If FindQ then With SearchForm do If f2.Checked and (Length(TfEd.Text) > 1) then
 begin
  s1 := TfEd.Text;
  tp:=0;
  msize:=length(s1);
searchresform.searchlist.Clear;
 l1:
  f:=true;
  if msize+tp<=romsize then
  begin
   for i:=1 to msize-1 do
    begin
      k1:=ord(s1[i])-RomData^[i+tp];
      k2:=ord(s1[i+1])-RomData^[i+tp+1];
      if k1<>k2 then f:=false;
    end;
   tp:=tp+1;
   if f then
    searchresform.searchlist.Items.Add(inttohex(tp, 8)+' '+s1+'=' + rgethex(tp, msize));
   goto l1;
  end;
  searchresform.Show;
 end;
end;


procedure TDjinnTileMapper.SpinEdit1Change(Sender: TObject);
begin
 maxSLen := SpinEdit1.Value;
 MaxLenLab.Caption := '������������ �����: ' +
  IntToStr(MaxSlen) + ' = h0' + IntToHex(maxSlen, 3);
end;

procedure TDjinnTileMapper.ottnlClick(Sender: TObject);
Label Error, EndProc;
Var T: TextFile; s: String; err: Boolean; n: Byte; backup: TTileTable; p: LongInt;
begin
 If OpenTBL.Execute then
 begin
  tTBLname := openTBL.FileName;
  AssignFile(t, tTBLname);
  Reset(t);
  Readln(t, s);
  If UpCase(s[1]) = 'H' then
  begin
   p := HexVal(Copy(s, 2, Length(s) - 1), err);
{   If Err then Goto Error;}
   If err then p := 0;
  end Else
  begin
   p := StrToInt(s);
   If p < 0 then {Goto Error} p := 0;
  end;
  For n := 0 to 255 do Backup[n] := 0;
  While not EOF(t) do
  begin
   Readln(t, s);
   n := HexVal(copy(s, 1, 2), err);
{   If Err or (s[3] <> '=') then Goto Error;}
   backup[n] := HexVal(copy(s, 4, 2), err);
   If not Err and (length(s) > 3) and (s[3] = '=') then
    backup[n] := HexVal(copy(s, 4, 2), err);
   If copy(s, 4, 2) = '' then
   begin
    backup[n] := 0;
    Err := False;
   end;
{   If Err then Goto Error;}
  end;
  CloseFile(t);
  tTBLopened := True;
  TileTable := backup;
  Label4.Caption := '������� ������: ' + tTBLname;
  RtTBL.Enabled := True;
  CtTBL.Enabled := True;
  if ROMopened then
  begin
   tilemapform.TMapScroll.Position := p;
   DataMapDraw;
   Bank := TileTable[RomData^[romDatapos + CDPos]];
  end;
 end;
 Goto EndProc;
Error:
 CloseFile(t);
{ ShowMessage('�������� ������ �����!!!');}
EndProc:
end;

procedure TDjinnTileMapper.cttblClick(Sender: TObject);
begin
 tTblopened := False;
 Label4.Caption := '������� ������:';
 cttbl.Enabled := False;
 rttbl.Enabled := False;
 if ROMopened then
 begin
  DataMapDraw;
  Bank := RomData^[romDatapos + CDPos];
 end;
end;

procedure TDjinnTileMapper.rttblClick(Sender: TObject);
Label Error, EndProc;
Var T: TextFile; s: String; err: Boolean; n: Byte; backup: TTileTable; p: LongInt;
begin
 If not FileExists(tTBLname) then
 begin
  AssignFile(t, tTBLname);
  Rewrite(t);
  For n := 0 to 255 do Writeln(t, IntToHex(n, 2) + '=' + IntToHex(TileTable[n], 2));
  CloseFile(t);
  Exit;
 end;
  AssignFile(t, tTBLname);
  Reset(t);
  Readln(t, s);
  If UpCase(s[1]) = 'H' then
  begin
   p := HexVal(Copy(s, 2, Length(s) - 1), err);
   If Err then Goto Error;
  end Else
  begin
   p := StrToInt(s);
   If p < 0 then Goto Error;
  end;
  For n := 0 to 255 do Backup[n] := 0;
  While not EOF(t) do
  begin
   Readln(t, s);
   n := HexVal(copy(s, 1, 2), err);
   If Err or (s[3] <> '=') then Goto Error;
   backup[n] := HexVal(copy(s, 4, 2), err);
   If copy(s, 4, 2) = '' then
   begin
    backup[n] := 0;
    Err := False;
   end;
   If Err then Goto Error;
  end;
  CloseFile(t);
  tTBLopened := True;
  TileTable := backup;
  Label4.Caption := '������� ������: ' + tTBLname;
  if ROMopened then
  begin
   tilemapform.TMapScroll.Position := p;
   DataMapDraw;
   Bank := TileTable[RomData^[romDatapos + CDPos]];
  end;
 Goto EndProc;
Error:
 CloseFile(t);
 ShowMessage('�������� ������ �����!!!');
EndProc:
end;

procedure TDjinnTileMapper.N3Click(Sender: TObject);
begin
 ShowMessage(
 '������� ������' + #10#13#10#13 +
 '������� ��������:' + #10#13#9 +
 '������ ������ - ������� ����� ������ � ROM''�;' + #10#13#9 +
 '��������� ������ - ���������� ����������� ������� ��������.' + #10#13 +
 '������:'#13#10#9'H020010'#10#13#9'0B=shi'#10#13#9'0C=su'#10#13#10#13+
 '������� ������:' + #10#13#9 +
 '������ ������ - ������� ����� ������ � ROM''�;' + #10#13#9 +
 '��������� ������ - ������ ������ ��� ������� ��������.' + #10#13 +
 '������:'#13#10#9'H027510'#10#13#9'19=3F'#10#13#9'1A=40');
end;

procedure TDjinnTileMapper.LCntClick(Sender: TObject);
Var n: Integer;
begin
 lCNt.checked := not LCnt.checked;
 If LCnt.Checked then n := length(DataEdit.Text) Else n := GetLength;
 LenLab.Caption := '�����: ' +
  IntToStr(n) + ' = h0' + IntToHex(n, 3);
end;

Procedure ddd16;
Var p: PROMData; a, b, c, d: Byte;
Procedure getbitcol;
var i: byte; cn: byte;
begin
 cn := 0;
 for i := 0 to 15 do
  if btile.canvas.Pixels[tx, ty] = Col16[i]then
  begin
   cn := i;
   Break;
  end;
 a := (cn shr 3) and 1;
 b := (cn shr 2) and 1;
 c := (cn shr 1) and 1;
 d := (cn shr 0) and 1;
end;
var xu: integer;
begin
 If (tx >= 0) and (ty >= 0) and (tx < 16) and (ty < 16) then
 With DjinnTileMapper do
 begin
  p := Addr(ROMData^[DataPos + Bank * 32 * bsz[Codec]]);
  Case Codec of
   cdc1BPP:
   If bTile.Canvas.Pixels[tx, ty] = Colors[1] then
   begin
    p^[ty * 2 + tx div 8] := p^[ty * 2 + tx div 8] and not (1 shl (7 - (tx mod 8)))
   end Else
   begin
    p^[ty * 2 + tx div 8] := p^[ty * 2 + tx div 8] or (1 shl (7 - (tx mod 8)))
   end;
   cdc2BPPNES:
   begin
    xu := ty + 16 * (tx div 8) + 24 * (ty div 8);
    If bTile.Canvas.Pixels[tx, ty] = Colors[1] then
    begin
     p^[xu] := p^[xu] and not (1 shl (7 - tx mod 8));
     p^[xu + 8] := p^[xu + 8] and not (1 shl (7 - tx mod 8))
    end Else If bTile.Canvas.Pixels[tx, ty] = Colors[2] then
    begin
     p^[xu] := p^[xu] or (1 shl (7 - tx mod 8));
     p^[xu + 8] := p^[xu + 8] and not (1 shl (7 - tx mod 8))
    end Else If bTile.Canvas.Pixels[tx, ty] = Colors[4] then
    begin
     p^[xu] := p^[xu] or (1 shl (7 - tx mod 8));
     p^[xu + 8] := p^[xu + 8] or (1 shl (7 - tx mod 8))
    end Else
    begin
     p^[xu] := p^[xu] and not (1 shl (7 - tx mod 8));
     p^[xu + 8] := p^[xu + 8] or (1 shl (7 - tx mod 8))
    END;
   end;
   cdc2BPPGB:
   begin
    xu := (ty mod 8) * 2 + 16 * (tx div 8) + 32 * (ty div 8);
    If bTile.Canvas.Pixels[tx, ty] = Colors[1] then
    begin
     p^[xu] := p^[xu] and not (1 shl (7 - tx mod 8));
     p^[xu + 1] := p^[xu + 1] and not (1 shl (7 - tx mod 8))
    end Else If bTile.Canvas.Pixels[tx, ty] = Colors[2] then
    begin
     p^[xu] := p^[xu] or (1 shl (7 - tx mod 8));
     p^[xu + 1] := p^[xu + 1] and not (1 shl (7 - tx mod 8))
    end Else If bTile.Canvas.Pixels[tx, ty] = Colors[4] then
    begin
     p^[xu] := p^[xu] or (1 shl (7 - tx mod 8));
     p^[xu + 1] := p^[xu + 1] or (1 shl (7 - tx mod 8))
    end Else
    begin
     p^[xu] := p^[xu] and not (1 shl (7 - tx mod 8));
     p^[xu + 1] := p^[xu + 1] or (1 shl (7 - tx mod 8))
    END;
   end;
  end;
 end;
end;


procedure TDjinnTileMapper.N4Click(Sender: TObject);
begin
 tilemapform.leftbtnclick(tilemapform.leftbtn);
end;

procedure TDjinnTileMapper.N5Click(Sender: TObject);
begin
 tilemapform.rightbtnclick(tilemapform.rightbtn);
end;

procedure TDjinnTileMapper.N6Click(Sender: TObject);
begin
 tilemapform.minusbtnclick(tilemapform.minusbtn);
end;

procedure TDjinnTileMapper.N7Click(Sender: TObject);
begin
 tilemapform.plusbtnclick(tilemapform.plusbtn);
end;

procedure TDjinnTileMapper.N8Click(Sender: TObject);
begin
 teditform.clearbtnclick(teditform.clearbtn);
end;

procedure TDjinnTileMapper.N9Click(Sender: TObject);
begin
 dataform.dleftbtnclick(dataform.dleftbtn);
end;

procedure TDjinnTileMapper.N10Click(Sender: TObject);
begin
 dataform.drightbtnclick(dataform.drightbtn);
end;

procedure TDjinnTileMapper.tmpanDockOver(Sender: TObject;
  Source: TDragDockObject; X, Y: Integer; State: TDragState;
  var Accept: Boolean);
begin
// accept := false;
end;

procedure TDjinnTileMapper.N11Click(Sender: TObject);
begin
 n11.checked := not n11.checked;
 tilemapform.Visible := n11.checked;
end;

procedure TDjinnTileMapper.N12Click(Sender: TObject);
begin
 n12.checked := not n12.checked;
 teditform.Visible := n12.checked;
end;

procedure TDjinnTileMapper.N13Click(Sender: TObject);
begin
 n13.checked := not n13.checked;
 dataform.otherpan.Visible := n13.checked;
end;

procedure TDjinnTileMapper.optItmClick(Sender: TObject);
begin
 if not tilemapform.Visible then n11.Checked := false;
 if not teditform.Visible then n12.Checked := false;
 if not dataform.otherpan.Visible then n13.Checked := false;
end;

procedure TDjinnTileMapper.BMP1Click(Sender: TObject);
var v: tbitmap; bb, i, x1, y1, xx, yy, xu, yu: integer; b: byte;
label endc;
begin
 OpenPal.Filter := 'Bitmap (*.bmp)|*.bmp';
 OpenPal.defaultext := '.bmp';
 OpenPal.title := '������ �� BMP';
 If OpenPal.Execute then
 begin
  v := tbitmap.Create;
  v.LoadFromFile(openpal.FileName);
  bmap.Canvas.CopyRect(Bounds(0, 0, v.Width, v.Height), v.Canvas,
                       Bounds(0, 0, v.Width, v.Height));
  v.Width := bmap.Width;
  v.Height := bmap.Height;
  i := curbank;
  if (tlwidthv = 1) and (tlheightv = 1) then
  For bb := 0 to 255 do
  begin
   curbank := bb;
   bTile.Canvas.CopyRect(Bounds
   (0, 0, tilew + 8 * Byte(b16x16), tileh + 8 * Byte(b16x16)),
    bmap.Canvas,
    Bounds((bb mod 16) * (tilew + 8 * byte(b16x16)),
    (bb div 16) * (tileh + 8 * byte(b16x16)), tilew + 8 * Byte(b16x16), tileh + 8 * Byte(b16x16)));
   for yy := 0 to tileh + 8 * Byte(b16x16) - 1 do
    for xx := 0 to tilew + 8 * Byte(b16x16) - 1 do
    begin
     tx := xx;
     ty := yy;
     ddd;
    end;
  end else
  begin
{  For bb := 0 to 255 do
  begin          }
   bb := 0;
   curbank := bb;
   for yy := 0 to 16 div tlheightv - 1 do
    for xx := 0 to 16 div tlwidthv - 1 do
    begin
     for yu := 0 to tlheightv - 1 do
      for xu := 0 to tlwidthv - 1 do
      begin
  v.Canvas.CopyRect(Bounds((bb mod 16)
  shl (3 + Byte(b16x16)),
   (bb div 16) shl (3 + Byte(b16x16)),
   8 + 8 * Byte(b16x16), 8 + 8 * Byte(b16x16)), bmap.Canvas,
   Bounds((xx *  tlwidthv + xu) shl (3 + Byte(b16x16)),
   (yy *  tlheightv + yu) shl (3 + Byte(b16x16)), 8 + 8 * Byte(b16x16),
    8 + 8 * Byte(b16x16)));
   bTile.Canvas.CopyRect(Bounds(0, 0, 8 + 8 * Byte(b16x16), 8 + 8 * Byte(b16x16)), v.Canvas,
            Bounds((bb mod 16) shl (3 + Byte(b16x16)),
            (bb div 16) shl (3 + Byte(b16x16)), 8 + 8 * Byte(b16x16), 8 + 8 * Byte(b16x16)));
   for y1 := 0 to 8 + 8 * Byte(b16x16) - 1 do
    for x1 := 0 to 8 + 8 * Byte(b16x16) - 1 do
    begin
     tx := x1;
     ty := y1;
     ddd;
    end;
{      goto endc;
       end;}
       inc(bb);
       curbank := bb;
      end;
    end;
  end;
  bmap.Canvas.CopyRect(Bounds(0, 0, v.Width, v.Height), v.Canvas,
                       Bounds(0, 0, v.Width, v.Height));
  v.Free;
  bank := i;
  Saved := false;
 Caption := Application.Title + ' - [' + fname + ']' +
            SS[Saved];
 end;
 OpenPal.Filter := '������� (*.pal)|*.pal|��� ����� (*.*)|*.*';
 OpenPal.defaultext := '.pal';
 OpenPal.title := '�������� �������';
end;

procedure TDjinnTileMapper.BMP2Click(Sender: TObject);
var bbb: tbitmap; b: byte; xx, yy, xu, yu: integer;
begin
 SavePal.Filter := 'Bitmap (*.bmp)|*.bmp';
 SavePal.defaultext := '.bmp';
 SavePal.title := '������� � BMP';
 If SavePal.Execute then
 begin
  If (tlwidthv = 1) and (tlheightv=1) then
  begin
   bmap.PixelFormat := pf24bit;
   bmap.SaveToFile(savepal.FileName);
  end else
  begin
   bbb := tbitmap.Create;
   bbb.PixelFormat := pf24bit;
   bbb.Width := bmap.Width;
   bbb.Height := bmap.Height;
  b := 0;
  for yy := 0 to 16 div tlheightv - 1 do
   for xx := 0 to 16 div tlwidthv - 1 do
   begin
    for yu := 0 to tlheightv - 1 do
     for xu := 0 to tlwidthv - 1 do
     begin
bbb.Canvas.CopyRect(Bounds(
    (xx *  tlwidthv + xu) * (8 + 8 * Byte(b16x16)),
    (yy * tlheightv + yu) * (8 + 8 * Byte(b16x16)),
    8 + 8 * Byte(b16x16), 8 + 8 * Byte(b16x16)), bmap.Canvas,
    Bounds((b mod 16) shl (3 + Byte(b16x16)),
    (b div 16) shl (3 + Byte(b16x16)), 8 + 8 * Byte(b16x16),
    8 + 8 * Byte(b16x16)));
      inc(b);
     end;
   end;
   bbb.SaveToFile(savepal.FileName);
   bbb.Free;
  end;
 end;
 SavePal.Filter := '������� (*.pal)|*.pal|��� ����� (*.*)|*.*';
 SavePal.defaultext := '.pal';
 SavePal.title := '���������� �������';
end;

procedure TDjinnTileMapper.vstrfontClick(Sender: TObject);
begin
 vstrfont.Checked := not vstrfont.Checked;
 if romopened then
 datamapdraw;
end;

//DUMP SCRIPT
procedure TDjinnTileMapper.N20Click(Sender: TObject);
var script: textfile; line: string; i: integer; ps: byte;
begin
 crtransform.dmp := true;
 crtransform.Caption := '������� �����';
 crtransform.radiobutton1.Visible := false;
 crtransform.radiobutton1.checked := true;
 crtransform.radiobutton2.Visible := false;
 crtransform.CheckBox1.Checked := false;
 crtransform.CheckBox1.Caption := '������ �������� ������� �������';
 crtransform.Label1.Visible := true;
 crtransform.showmodal;
 if crtransform.ok then
 begin
  assignfile(script, crtransform.dd.FileName);
  rewrite(script);
  if (num2 < num1) then begin closefile(script); showmessage('������!!!') end;
  line := ''; if num2 >= romsize then num2 := romsize - 1;
{  if rzero then writeln(script, '!!!');
  writeln(script, Inttohex(num1, 8));}
  for i := num1 to num2 do
  begin
   ps := romdata^[i];
   if ps = perenos then
   begin
    writeln(script, line);
    line := '';
    if i = num2 then
    begin
     writeln(script, '{END}');
     writeln(script);
    end;
   end Else if ps = endt then
   begin
    line := line + '{END}';
    writeln(script, line);
    writeln(script);
{    if rzero and (i < num2) then writeln(script, Inttohex(i + 1, 8));}
    line := '';
   end Else
   begin
    line := line + table[ps];
    if i = num2 then
    begin
     writeln(script, line);
     line := '';
    end;
   end;
  end;
  closefile(script);
  showmessage('��������� ' + inttostr(num2 - num1 + 1) + ' ����.');
 end;
end;

//REPLACE SCRIPT
procedure TDjinnTileMapper.N24Click(Sender: TObject);
var script: textfile; line: string; i: integer; 
    scrt: tstrings; n, l, j, k: Integer; s: String;

Function cfind(p: Integer): Integer;
Var g: Integer; n: Byte;
begin
 Result := -1;
 For g := maxtlen downto 1 do
  For n := 0 to 255 do If Copy(s, p, g) = Table[n] then
  begin
   Result := n;
   Exit;
  end;
end;
label laba;
begin
 crtransform.dmp := false;
 crtransform.Caption := '�������� �����';
 crtransform.radiobutton1.Visible := true;
 crtransform.radiobutton1.checked := true;
 crtransform.radiobutton2.Visible := {true}false;
 crtransform.CheckBox1.Checked := false;
 crtransform.CheckBox1.Caption := '��������� ��������� ������';
 crtransform.Label1.Visible := false;
 crtransform.showmodal;
 if crtransform.ok then
 begin
  assignfile(script, crtransform.rr.FileName);
  reset(script);
  scrt := tstringlist.Create;
  while not eof(script) do
  begin
   readln(script, line);
   scrt.Add(line);
  end;
  s := scrt.GetText;
  k := 0;
  for j := 1 to scrt.Count do
  begin
   s := scrt.Strings[j - 1];
   i := 1; l := 0;
   while i <= Length(s) do
   begin
    n := cfind(i);
    If n >= 0 then
    begin
     inc(i, Length(Table[n]) - 1);
     ROMData^[num1 + k + l] := n;
     Inc(l);
    end;
    Inc(i);
   end;
   inc(k, l);
   if (ROMData^[num1 + k - 1] <> endt) OR
      ((S = '') AND ((J = 1) OR
      ((ROMData^[num1 + k - 1] = ENDT) AND (scrt.Strings[j - 2] = '')))) then
   begin
    ROMData^[num1 + k] := PERENOS;
    inc(k);
   end;
  end;
  scrt.Free;
  drawtilemap;
  closefile(script);
 end;
end;

procedure TDjinnTileMapper.N25Click(Sender: TObject);
begin
 searchresform.show;
end;

procedure TDjinnTileMapper.initposes;
begin
 top := 0;
 left := 0;
 tilemapform.Top := 0;
 tilemapform.left := 544;
 dataform.top := 116;
 dataform.left := 0;
 teditform.Top := 350;
 teditform.left := 544;
end;

procedure TDjinnTileMapper.N27Click(Sender: TObject);
begin
 initposes;
end;

procedure TDjinnTileMapper.N28Click(Sender: TObject);
var b: byte; i: longword;
begin
 b := strtoint(InputBx(Application.Title, '������� ����������� ����', ['0'..'9', #13, #8], 3));
 if not iokresult then exit;
 for i := romdatapos + cdpos to romdatapos + cdpos + maxSlen - 1 do romdata^[i] := b;
 drawtilemap;
end;

end.

